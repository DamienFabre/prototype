#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import time
import subprocess
# import shlex
import configparser
import shutil

if not os.path.isfile(".project.cfg"):
    sys.exit("Le fichier le fichier de configuration est manquant !")

config = configparser.ConfigParser()
config.read('.project.cfg')

root_folder = config.get('path', 'root')
src_folder = os.path.join(root_folder, config.get('path', 'source'))
build_folder = os.path.join(root_folder, config.get('path', 'build'))
prog = config.get('build', 'prog_name')

class Counter:
    def __init__(self):
        self.debut = time.time()

    def stop(self):
        fin = time.time()
        return str(round(fin - self.debut, 3))

def backup_project():
    counter = Counter()
    if os.getenv('PWD') != root_folder:
        subprocess.call("cd {0}".format(root_folder))
    subprocess.call("./tools/backup_project.sh", shell=True)
    print("Sauvegarde exécuter en {} secondes...".format(counter.stop()))

def clean_directory(path = None):
    counter = Counter()
    subprocess.call("make distclean", shell=True)
    print("Nettoyage exécuter en {} secondes...".format(counter.stop()))

def analyze_source():
    counter = Counter()
    subprocess.call("./tools/analyze.sh {0}".format(src_folder), shell=True)
    print("Analyse exécuter en {} secondes...".format(counter.stop()))

def make_doc():
    counter = Counter()
    src = root_folder + "/build/doc/html"
    dst = root_folder + "/doc"
    subprocess.call("make doc", shell=True)
    shutil.move(src, dst)
    print("Documentation créer en {} secondes...".format(counter.stop()))

def nb_ligne():
    r = subprocess.getoutput("./tools/nb_ligne.sh")
    print("Nombre de lignes \n" + r)

def update_cmake(param=""):
    info = str()
    buildType = str()
    if param == "r":
        info, buildType = "release...", "Release"
    elif param == "hard":
        info, buildType = "Debug...\n\nATTENTION HARDCORE !!!\n\n", "hardcore"
    elif param == "nw":
        info, buildType = "Debug...\nSuppression des warnings...", "no_warnings"
    else:
        info, buildType = "debug...", "Debug"
    print("Mise à jour des fichiers de cmake en mode {}...\n".format(info))
    os.system("cmake . -G\"Unix Makefiles\" -DCMAKE_BUILD_TYPE=\"{}\"".format(buildType))

def run_prog():
    subprocess.call("./bin/" + prog)

option = {
    "bak" : backup_project,
    "clean" : clean_directory,
    "check" : analyze_source,
    "doc" : make_doc,
    "line" : nb_ligne,
    "run" : run_prog,
}

if __name__ == "__main__":
    if sys.argv[1] == "update":
        update_cmake(sys.argv[2])
    else:
        try:
            option[sys.argv[1]]()
        except Exception as e:
            print("Error {0}".format(str(e)))

