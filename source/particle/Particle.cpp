#include "Particle.hpp"

Particle::Particle(sf::Vector2f position, sf::Vector2f velocity)
    : m_position(position)
    , m_velocity(velocity)
{
    m_color = sf::Color::White;
    m_lifeTime = sf::Time::Zero;
}