#ifndef PARTICLE_H
#define PARTICLE_H

#include <list>
#include <cassert>
#include <string>

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include "Particle.hpp"

class ParticleSystem : public sf::Drawable, public sf::Transformable
{
public:

    ParticleSystem(unsigned int count);
    ~ParticleSystem() = default;
 
    void setEmitter(sf::Vector2f position);
    void setEmitter(float x, float y);

    void update(sf::Time elapsed);
    void fuel(unsigned int nbParticles);

    void setGravity(float x, float y);
    void setGravity(sf::Vector2f gravity);

    void setParticleSpeed(float speed);
    void setParticleColor(sf::Color c);

    std::string getNumberOfParticles() const;

    void setDissolution(bool flag);
    bool getDissolutionFlag() const;

private:

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    struct Particle
    {
        sf::Vector2f    velocity;
        sf::Time        lifetime;
        sf::Color       color;
    };

    void resetParticle(unsigned int index);

    std::vector<Particle>   m_particles;
    sf::VertexArray         m_vertices;
    sf::Time                m_lifetime;
    sf::Vector2f            m_emitter;
    sf::Vector2f            m_gravity;
    float                   m_particleSpeed;
    sf::Color               m_particleColor;
    sf::Time                m_timeBeforeDisso;
    bool                    m_dissolution;
    bool                    m_stopDissolution;
};

#endif
