#include "ParticleSystem.hpp"

#include <sstream>
#include <iostream>
#include <cmath>

#include "../utility/Utility.hpp"

/////////////////////////////////////////////////////////////////////////////////
// namespace
// {
//  std::default_random_engine createRandomEngine()
//  {
//      auto seed = static_cast<unsigned long>(std::time(nullptr));
//      return std::default_random_engine(seed);
//  }

//  auto RandomEngine = createRandomEngine();
// }

// float randomFloat(float min, float max)
// {
//  std::uniform_real_distribution<> distr(min, max - 1);
//  return distr(RandomEngine);
// }

// int randomInt(int min, int max)
// {
//  std::uniform_int_distribution<> distr(min, max -1);
//  return distr(RandomEngine);
// }

// float length(sf::Vector2f vector)
// {
//  return std::sqrt(vector.x * vector.x + vector.y * vector.y);
// }

// sf::Vector2f unitVector(sf::Vector2f vector)
// {
//  if (vector == sf::Vector2f(0.f, 0.f))
//  {
//      Logger log;
//      log << Logger::LOG_ERROR << "le vecteur est égale à 0, 0";
//  }
//  assert(vector != sf::Vector2f(0.f, 0.f));
//  return vector / length(vector);
// }
/////////////////////////////////////////////////////////////////////////////////

ParticleSystem::ParticleSystem(unsigned int count) 
    : m_particles(count)
    , m_vertices(sf::Points, count)
    , m_lifetime(sf::seconds(3))
    , m_emitter(0.f, 0.f)
    , m_gravity(0.f, 0.f)
    , m_particleSpeed(100.f)
    , m_particleColor(sf::Color::White)
    , m_timeBeforeDisso(sf::Time::Zero)
    , m_dissolution(false)
    , m_stopDissolution(false)
{
}

void ParticleSystem::setEmitter(sf::Vector2f position)
{
    m_emitter = position;
}

void ParticleSystem::setEmitter(float x, float y)
{
    this->setEmitter({x, y});
    // m_emitter.x = x;
    // m_emitter.y = y;
}

void ParticleSystem::update(sf::Time elapsed)
{
    // on supprime des particules au bout d'un certain temps
    // si m_stopDissolution n'est pas vrai
    if (m_dissolution && !m_stopDissolution)
    {
        // on calcul par rapport au temps le nombre de particules à supprimer
        int rate = static_cast<int>(std::trunc(500.f * elapsed.asSeconds()));
        if ((m_particles.size() - rate) > 2 && m_vertices.getVertexCount() - rate > 2)
        {
            // on supprime i élément dans m_particles (std::vector<Particle>)
            for (int i = 0; i < rate; i++)
                m_particles.pop_back();

            // on supprime également i éléments dans le tableau de vertex
            m_vertices.resize(m_vertices.getVertexCount() - rate);
        }
    } 
    else if (!m_stopDissolution)
    {
        // si le temps est supérieur on active le flag de dissolution
        m_timeBeforeDisso += elapsed;
        if (m_timeBeforeDisso >= sf::seconds(3.f))
            m_dissolution = true;
    }

    for (unsigned int i = 0; i < m_particles.size(); i++)
    {
        // on met à jour la durée de vie de la particule
        Particle& p = m_particles[i];
        p.lifetime -= elapsed;

        // si la particule est arrivée en fin de vie, on la réinitialise
        if (p.lifetime <= sf::Time::Zero)
            resetParticle(i);

        // on met à jour la position du vertex correspondant
        m_vertices[i].position += unitVector(p.velocity + m_gravity) 
                                    * m_particleSpeed * elapsed.asSeconds();

        // on met à jour l'alpha (transparence) de la particule en fonction de sa durée de vie
        float ratio = p.lifetime.asSeconds() / m_lifetime.asSeconds();
        m_vertices[i].color = m_particleColor;
        m_vertices[i].color.a = static_cast<sf::Uint8>(ratio * 255);
    }
}

void ParticleSystem::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    // on applique la transformation
    states.transform *= getTransform();

    // nos particules n'utilisent pas de texture
    states.texture = nullptr;

    // on dessine enfin le vertex array
    target.draw(m_vertices, states);
}

void ParticleSystem::resetParticle(unsigned int index)
{
    // on donne une vitesse et une durée de vie aléatoires à la particule
    float angle = static_cast<float>((std::rand() % 360)) * 3.14f / 180.f;
    float speed = static_cast<float>((std::rand() % 50)) + 50.f;

    m_particles[index].velocity = sf::Vector2f(std::cos(angle) * speed, 
                                                            std::sin(angle) * speed);
    m_particles[index].lifetime = sf::milliseconds((std::rand() % 2000) + 1000);

    // on remet à zéro la position du vertex correspondant à la particule
    m_vertices[index].position = m_emitter;
}

std::string ParticleSystem::getNumberOfParticles() const
{
    std::ostringstream oss;
    oss << m_particles.size();
    return oss.str();
}

void ParticleSystem::setGravity(float x, float y)
{
    m_gravity.x = x;
    m_gravity.y = y;
}

void ParticleSystem::setGravity(sf::Vector2f gravity)
{
    m_gravity = gravity;
}

void ParticleSystem::setParticleSpeed(float speed)
{
    m_particleSpeed = speed;
}

void ParticleSystem::setParticleColor(sf::Color c)
{
    m_particleColor = c;
}

void ParticleSystem::fuel(unsigned int nbParticles)
{
    // on réinitialise la dissolution des particules
    m_timeBeforeDisso = sf::Time::Zero;
    m_dissolution = false;

    for (unsigned int i = 0; i < nbParticles; i++)
    {
        Particle p;

        // on donne une vitesse et une durée de vie aléatoires à la particule
        float angle = static_cast<float>((std::rand() % 360)) * 3.14f / 180.f;
        float speed = static_cast<float>((std::rand() % 50)) + 50.f;

        p.velocity = sf::Vector2f(std::cos(angle) * speed, std::sin(angle) * speed);
        p.lifetime = sf::milliseconds((std::rand() % 2000) + 1000);

        m_particles.push_back(p);
        m_vertices.resize(m_vertices.getVertexCount() + 1);
        m_vertices[m_vertices.getVertexCount()-1].position = m_emitter;
    }
}

void ParticleSystem::setDissolution(bool flag)
{
    m_stopDissolution = flag;
}

bool ParticleSystem::getDissolutionFlag() const
{
    return m_stopDissolution;
}