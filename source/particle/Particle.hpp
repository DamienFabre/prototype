#ifndef PARTICLE_HPP
#define PARTICLE_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

struct Particle
{
    Particle(sf::Vector2f position, sf::Vector2f velocity);
    ~Particle() = default;
 
    sf::Vector2f m_position; // Position
    sf::Vector2f m_velocity; // Direction
    sf::Color m_color; // RGBA
    sf::Time m_lifeTime;
}; 

#endif // PARTICLE_HPP