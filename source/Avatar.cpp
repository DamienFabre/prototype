#include "Avatar.hpp"

#include "events/Command.hpp"
#include "lua/LuaContext.hpp"

Avatar::Avatar(TextureHolder& textures)
    : IEntity(0)
    , m_sprite(textures.get("player"))
    , m_speed(200.f)
    , m_position(400.f, 300.f)
    , m_velocity(1.f, 0.f)
    , m_luaContext(LuaContext::getInstance())
{
    m_sprite.setPosition(m_position);
    m_category = Category::Player;
    m_luaContext->doScript("scripts/playerAction.lua");

    LuaContext::pushPointer(m_luaContext->getLuaVM(), this);
    luaL_getmetatable(m_luaContext->getLuaVM(), "luaL_avatar");
    lua_setmetatable(m_luaContext->getLuaVM(), -2);
    lua_setglobal(m_luaContext->getLuaVM(), "avatar");
}

void Avatar::update(sf::Time)
{
    m_position = m_sprite.getPosition();
    // m_sprite.move(m_velocity * m_speed * dt.asSeconds());
}

void Avatar::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(m_sprite, states);
}

float Avatar::getSpeed() const
{
    return m_speed;
}

sf::Vector2f Avatar::getPosition() const
{
    return m_position;
}

void Avatar::setSpeed(float speed)
{
    m_speed = speed;
}

void Avatar::setPosition(sf::Vector2f position)
{
    m_position = position;
}

void Avatar::setPosition(float x, float y)
{
    m_position = sf::Vector2f(x, y);
}

sf::Sprite Avatar::getSprite() const
{
    return m_sprite;
}

void Avatar::move(const sf::Vector2f& offset)
{
    m_sprite.move(offset * m_speed);
}

void Avatar::onCommand(const Command& command, sf::Time dt)
{
    if (command.category & getCategory())
        command.action(this, dt);
}

void Avatar::fire()
{
    m_luaContext->on_shoot(this);
}