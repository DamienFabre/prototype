#include <cstdlib>
#include <sstream>

#include <physfs.h>

#include "Application.hpp"
#include "utility/Logger.hpp"

// #include <X11/Xlib.h>

int main(int, char** /*const *argv[]*/)
{
    // Activation du multi-threading
    // XInitThreads();

    // création du logger
    Logger log;
    log.setLogInfo("Prototype 1.0");
    log << Logger::LOG_INFO << "Lancement de l'application";

    // log << Logger::LOG_INFO << "PhysFs : Initialisation";
    // PHYSFS_init(argv[0]);
    // if (PHYSFS_addToSearchPath("assets.zip", 1) == 0)
    // {
    //     std::cout << "PhysFs : Erreur de chargement de l'archive assets.zip" << std::endl;
    //     log << Logger::LOG_ERROR << "PhysFs : Erreur de chargement de l'archive assets.zip";
    //     return EXIT_FAILURE;
    // }

    try 
    {
        Application app;
        app.init();
        app.run();
    } 
    catch (std::exception& e) 
    {
        std::ostringstream oss;
        oss << "EXCEPTION : " << e.what();
        log << Logger::LOG_ERROR << oss.str();
    } 

    // log << Logger::LOG_INFO << "PhysFs : Fermeture";
    // PHYSFS_deinit();

    log << Logger::LOG_INFO << "Fermeture de l'application";
    log.close();

    return EXIT_SUCCESS;
}