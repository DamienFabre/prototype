#ifndef AVATAR_HPP_INCLUDED
#define AVATAR_HPP_INCLUDED

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>

#include "utility/Logger.hpp"
#include "utility/NonCopyable.hpp"
#include "resources/ResourceIdentifiers.hpp"
#include "resources/ResourceHolder.hpp"
#include "ecs/IEntity.hpp"

class Command;
class LuaContext;

class Avatar : private NonCopyable, public IEntity
{
public:

    Avatar(TextureHolder& textures);
    ~Avatar() = default;

    void update(sf::Time dt);
    void move(const sf::Vector2f& offset);
    void onCommand(const Command& command, sf::Time dt);

    // Action
    void fire();

    // Getter
    float getSpeed() const;
    sf::Vector2f getPosition() const;
    sf::Sprite getSprite() const;

    // Setter
    void setSpeed(float speed);
    void setPosition(sf::Vector2f position);
    void setPosition(float x, float y);

private:

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    sf::Sprite m_sprite;
    float m_speed = 10,f;
    sf::Vector2f m_position;
    sf::Vector2f m_velocity;
    LuaContext* m_luaContext;
};

#endif // AVATAR_HPP_INCLUDED