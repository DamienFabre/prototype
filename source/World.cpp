#include "World.hpp"

#include <iostream>

#include <SFML/Graphics/RenderWindow.hpp>

World::World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, 
             TextureHolder& textures, LuaContext& luaContext)
    : m_target(outputTarget)
    , m_textures(textures)
    , m_fonts(fonts)
    , m_sounds(sounds)
    , m_luaContext(luaContext)
    , m_worldView()
    , m_components()
    , m_systemsMgr(m_components)
    , m_entityMgr(m_systemsMgr)
    , m_worldMap()
    , m_avatar(textures)
    , m_commandQueue()
{
}

void World::init()
{
    m_components.init();
    m_systemsMgr.init();

    m_luaContext.addUserData("entity_mgr", &m_entityMgr);
    m_luaContext.addUserData("map", &m_worldMap);
    // m_luaContext.addUserData("avatar", &m_avatar);

    m_luaContext.doScript("scripts/level/level_test.lua");
    // m_luaContext.callFunction("entity_test_1", "init");
    // m_luaContext.callFunction("entity_test_2", "init");
}

void World::update(sf::Time dt)
{
    while (not m_commandQueue.isEmpty())
    {
        Command command = m_commandQueue.pop();
        m_avatar.onCommand(command, dt);
        m_entityMgr.onCommand(command, dt);
    }

    m_avatar.update(dt);
    m_entityMgr.update(dt);
    // std::vector<IEntity*> entities = m_entityMgr.getEntityList();
    // m_systemsMgr.update(dt, entities);
}

void World::draw()
{
    m_target.draw(m_worldMap);
    for (const auto& e : m_entityMgr.getEntityList())
    {
        if (e->hasComponent(Component::ID::Sprite))
        {
            sf::Sprite& sprite = e->getComponent<Sprite>(Component::ID::Sprite)->sprite;
            m_target.draw(sprite);
        }
        else if (e->hasComponent(Component::ID::Shape))
        {
            sf::Shape& shape = e->getComponent<Shape>(Component::ID::Shape)->shape;
            m_target.draw(shape);
        }
    }
    m_target.draw(m_avatar.getSprite());
}

CommandQueue& World::getCommandQueue()
{
    return m_commandQueue;
}