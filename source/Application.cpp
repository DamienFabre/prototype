#include "Application.hpp"

#include <ctime>
#include <string>
#include <iostream>
#include <chrono>
#include <string>
#include <array>
// #include <thread>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/System/Time.hpp>

#include "states/StateList.hpp"

// initialisation des variables statics
const sf::Time Application::TimePerFrame = sf::seconds(1.f/60.f);

/**
 * Constructeur
 */
Application::Application()
    : m_window()
    , m_resourceManager()
    , m_player()
    , m_luaContext()
    , m_stateManager(State::Context(m_window, 
                                    m_resourceManager.getTextureHolder(), 
                                    m_resourceManager.getFontHolder(), 
                                    m_player, 
                                    m_resourceManager.getMusicPlayer(), 
                                    m_resourceManager.getSoudPlayer(), 
                                    m_luaContext))
    , m_isFocus(true)
    , m_isInit(false)
    , m_totalTime(0.f)
    , m_windowTitle()
{
}

/**
 * Initialise l'application
 */
void Application::init()
{
    LOG_INFO("Initialisation de l'application");
    
    m_luaContext.init();
    m_luaContext.addUserData("application", this);
    m_luaContext.addUserData("texture_holder", &m_resourceManager.getTextureHolder());
    m_luaContext.doScript("scripts/main.lua");

    m_resourceManager.init("assets", false);
    m_resourceManager.loadAllResources();

    // std::thread t(&Application::loadTextures, this);

    initWindow();

    registerStates();
    m_stateManager.pushState(States::ID::Game);

    // t.join();
    
    // m_resourceManager.getMusicPlayer().play("menu_theme");

    m_isInit = true;
}

/**
 * Lance la boucle principal du jeu
 */
void Application::run()
{
    if (not m_isInit) init();

    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    while (m_window.isOpen())
    {
        // récupère le temps actuel
        auto begin(std::chrono::high_resolution_clock::now());

        timeSinceLastUpdate += clock.restart();
        while (timeSinceLastUpdate > TimePerFrame)
        {
            timeSinceLastUpdate -= TimePerFrame;
            processEvents();

            // si la fenetre perd le focus on arrete d'update le jeu
            if (m_isFocus) update(TimePerFrame);
            if (m_stateManager.isEmpty()) m_window.close();
        }
        render();

        // on calcul le temps écoulé et on l'affiche dans la barre du programme
        auto end(std::chrono::high_resolution_clock::now());
        float frameTime = std::chrono::duration_cast<std::chrono::duration<float, std::milli>>(end - begin).count();
        m_totalTime += frameTime / 1000.f;
        float fps = (1.f / (frameTime / 1000.f));
        m_window.setTitle(m_windowTitle + "\t\tFT : " + std::to_string(frameTime) + "\tFPS : " + std::to_string(fps)
                          + "\tTemps : " + std::to_string(m_totalTime));
    }
}

/**
 * Traite tous les évènements et les délègues si besoin
 */
void Application::processEvents()
{
    sf::Event event;
    while (m_window.pollEvent(event))
    {
        m_stateManager.handleEvent(event);

        if ((event.type == sf::Event::Closed))
            m_window.close();
        if ((event.type == sf::Event::KeyPressed) and (event.key.code == sf::Keyboard::F2))
            takeScreenshot();
        if (event.type == sf::Event::GainedFocus) 
            m_isFocus = true;
        if (event.type == sf::Event::LostFocus)  
            m_isFocus = false;
        // if ((event.type == sf::Event::KeyPressed) and (event.key.code == sf::Keyboard::F3))
        //     m_displayStats = !m_displayStats;
    }
}

/**
 * Update l'appication
 * @param dt temps écoulé depuis le dernier appel
 */
void Application::update(sf::Time dt)
{
    m_stateManager.update(dt);
}

/**
 * Dessine la frame en cours
 */
void Application::render()
{
    // affichage de la souris
    sf::Sprite mouse(m_resourceManager.getTextureHolder().get("mouse"));
    mouse.setPosition(getMousePosition());
    mouse.setScale(1.5, 1.5);

    m_window.clear(sf::Color(70, 70, 70));
    m_stateManager.draw();
    m_window.draw(mouse);
    m_window.display();
}

/**
 * Fait une capture d'écran et l'enregistre sous la forme 
 * "sreenshot_Heure:Minute:seconde-Jour-Mois.jpg"
 */
void Application::takeScreenshot() const
{
    // on capture l'image actuel à l'écran
    sf::Image screenshot(m_window.capture());

    // on récupère le l'heure pour le nom du fichier
    char ltime[20];
    std::time_t t = std::time(nullptr);
    std::size_t len = std::strftime(ltime, 20, "%T-%d-%m", std::localtime(&t));

    // on sauvegarde l'image
    std::string filename("screenshot_");
    filename.append(ltime, len);
    screenshot.saveToFile(filename + ".jpg");
}

/**
 * Initialise la fenetre
 */
void Application::initWindow()
{
    lua_State *lvm = luaL_newstate();
    if (luaL_dofile(lvm, "settings.lua") != LUA_OK)
    {
        std::cout << "Erreur impossible de lire le fichier de configuration ! "
                  << lua_tostring(lvm, -1);
        std::exit(EXIT_FAILURE);
    }

    lua_getglobal(lvm, "width");
    lua_getglobal(lvm, "height");
    lua_getglobal(lvm, "framerateLimit");
    lua_getglobal(lvm, "vsync");
    lua_getglobal(lvm, "fullscreen");
    lua_getglobal(lvm, "title");

    m_window.create({luaL_checkunsigned(lvm, 1), luaL_checkunsigned(lvm, 2)}, 
        luaL_checkstring(lvm, 6), (lua_toboolean(lvm, 5) ? sf::Style::Fullscreen : sf::Style::Default));
    m_window.setFramerateLimit(luaL_checkunsigned(lvm, 3));
    m_window.setVerticalSyncEnabled(lua_toboolean(lvm, 4));
    m_windowTitle = luaL_checkstring(lvm, 6);

    lua_settop(lvm, 0);
    lua_close(lvm);

    m_window.setMouseCursorVisible(false);

    LOG_INFO("Initialisation de la fenetre");
} 

/**
 * Enregistre les différents states du jeu
 */
void Application::registerStates()
{
    // m_stateManager.registerStates<TitleState>(States::ID::Title);
    // m_stateManager.registerStates<PauseState>(States::ID::Pause);
    m_stateManager.registerStates<MenuState>(States::ID::Menu);
    m_stateManager.registerStates<GameState>(States::ID::Game);
    LOG_INFO("Enregistrement des états du jeu");
}

sf::Vector2f Application::getMousePosition() const
{
    sf::Vector2i position = sf::Mouse::getPosition(m_window);
    return static_cast<sf::Vector2f>(position);
}