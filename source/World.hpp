#ifndef WORLD_HPP_INCLUDED
#define WORLD_HPP_INCLUDED

#include <SFML/Graphics/View.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "ecs/ComponentFactory.hpp"
#include "ecs/SystemManager.hpp"
#include "ecs/EntityManager.hpp"
#include "lua/LuaContext.hpp"
#include "utility/NonCopyable.hpp"
#include "utility/Logger.hpp"
#include "resources/ResourceIdentifiers.hpp"
#include "resources/ResourceHolder.hpp"
#include "resources/MusicPlayer.hpp"
#include "resources/SoundPlayer.hpp"
#include "map/Map.hpp"
#include "Avatar.hpp"
#include "events/CommandQueue.hpp"

// Forward declaration
namespace sf
{
    class RenderTarget;
}

class World : NonCopyable
{
public:

    explicit World(sf::RenderTarget& outputTarget, FontHolder& fonts, SoundPlayer& sounds, 
                   TextureHolder& textures, LuaContext& luaContext);
    ~World() = default;

    void init();

    void update(sf::Time dt);
    void draw();

    // Getter
    CommandQueue& getCommandQueue();

private:

    sf::RenderTarget& m_target;
    TextureHolder&    m_textures;
    FontHolder&       m_fonts;
    SoundPlayer&      m_sounds;
    LuaContext&       m_luaContext;

    sf::View          m_worldView;
    ComponentFactory  m_components;
    SystemManager     m_systemsMgr;
    EntityManager     m_entityMgr;

    Map               m_worldMap;
    Avatar            m_avatar;

    CommandQueue      m_commandQueue;
};

#endif // WORLD_HPP_INCLUDED