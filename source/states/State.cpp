#include "State.hpp"
#include "StateManager.hpp"

State::Context::Context(sf::RenderTarget& ext_window, TextureHolder& ext_textures, 
                        FontHolder& ext_fonts, Player& ext_player, MusicPlayer& ext_music, 
                        SoundPlayer& ext_sounds, LuaContext& ext_luaContext)
    : window(&ext_window)
    , textures(&ext_textures)
    , fonts(&ext_fonts)
    , player(&ext_player)
    , music(&ext_music)
    , sounds(&ext_sounds)
    , luaContext(&ext_luaContext)
{
}

/// Constructeur
State::State(StateManager& stack, Context context)
: m_stack(&stack)
, m_context(context)
{
}

void State::requestStackPush(States::ID stateID)
{
    m_stack->pushState(stateID);
}

void State::requestStackPop()
{
    m_stack->popState();
}

void State::requestStateClear()
{
    m_stack->clearStates();
}

State::Context State::getContext() const
{
    return m_context;
}