#ifndef MENUSTATE_HPP_INCLUDED
#define MENUSTATE_HPP_INCLUDED

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>

#include "State.hpp"
#include "../gui/Container.hpp"

class MenuState : public State
{
public:

    MenuState(StateManager& stack, Context context);
    ~MenuState() = default;

    virtual void draw();
    virtual bool update(sf::Time dt);
    virtual bool handleEvent(const sf::Event& event);

private:

    sf::Sprite      m_backgroundSprite;
    GUI::Container  m_guiContainer;
};

#endif // MENUSTATE_HPP_INCLUDED