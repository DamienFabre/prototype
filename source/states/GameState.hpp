#ifndef GAMESTATE_HPP_INCLUED
#define GAMESTATE_HPP_INCLUED

#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>

#include "State.hpp"
#include "../World.hpp"
#include "../Player.hpp"

class GameState : public State
{
public:

    GameState(StateManager& stack, Context context);

    virtual void draw();
    virtual bool update(sf::Time dt);
    virtual bool handleEvent(const sf::Event& event);

private:

    // void updateGui();

    World   m_world;
    Player& m_player;
};

#endif // GAMESTATE_HPP_INCLUED