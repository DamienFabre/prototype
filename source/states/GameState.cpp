#include "GameState.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RectangleShape.hpp>

// #include "../resources/MusicPlayer.hpp"
// #include "../lua/ScriptInterface.hpp"

/// Constructeur
GameState::GameState(StateManager& stack, Context context)
    : State(stack, context)
    , m_world(*context.window, *context.fonts, *context.sounds, *context.textures, *context.luaContext)
    , m_player(*context.player)
{
    // m_player.setMissionStatus(Player::MissionRunning);
    m_world.init();
    // context.music->setVolume(20.f);
    // context.music->play(Music::ID::MissionTheme);
}

void GameState::draw()
{
    m_world.draw();
    // updateGui();
}

bool GameState::update(sf::Time dt)
{
    m_world.update(dt);

    CommandQueue& commands = m_world.getCommandQueue();
    m_player.handleRealTimeInput(commands);

    return true;
}

bool GameState::handleEvent(const sf::Event& event)
{
    CommandQueue& commands = m_world.getCommandQueue();
    m_player.handleEvent(event, commands);

    if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
        requestStackPush(States::ID::Pause);

    return true;
}