#include "StateManager.hpp"

#include <cassert>

/// Constructeur
StateManager::StateManager(State::Context context) 
    : m_stack()
    , m_pendingList()
    , m_context(context)
    , m_factories()
{
}

/**
 * @brief Transmet les update à tous les states
 * @param dt temps écouler depuis le dernier appel
 */
void StateManager::update(sf::Time dt)
{
    for (auto itr = m_stack.rbegin(); itr != m_stack.rend(); ++itr)
    {
        if (!(*itr)->update(dt))
            return;
    }

    applyPendingChanges();
}

/**
 * @brief Transmet l'ordre d'affichage à tous les states
 */
void StateManager::draw()
{
    for (State::Ptr& state : m_stack)
        state->draw();
}

/**
 * @brief Transmet les evenements à tous les states
 * @param event évenement à traiter
 */
void StateManager::handleEvent(const sf::Event& event)
{
    for (auto itr = m_stack.rbegin(); itr != m_stack.rend(); ++itr)
    {
        if (!(*itr)->handleEvent(event))
            break;
    }

    applyPendingChanges();
}

void StateManager::pushState(States::ID stateID)
{
    m_pendingList.push_back(PendingChange(Push, stateID));
}

void StateManager::popState()
{
    m_pendingList.push_back(PendingChange(Pop));
}

void StateManager::clearStates()
{
    m_pendingList.push_back(PendingChange(Clear));
}

bool StateManager::isEmpty() const
{
    return m_stack.empty();
}

/**
 * @brief Créer un nouveu state par rapport à l'id passer en paramètre
 * @param stateID id correspondent à un state du jeu
 * @return pointeur sur le state créer
 */
State::Ptr StateManager::createState(States::ID stateID)
{
    auto found = m_factories.find(stateID);
    assert(found != m_factories.end());

    return found->second();
}

void StateManager::applyPendingChanges()
{
    for (PendingChange change : m_pendingList)
    {
        switch (change.action)
        {
            case Push:
                m_stack.push_back(createState(change.stateID));
                break;

            case Pop:
                m_stack.pop_back();
                break;

            case Clear:
                m_stack.clear();
                break;

            default:
                break;
        }
    }

    m_pendingList.clear();
}

StateManager::PendingChange::PendingChange(Action ext_action, States::ID ext_stateID)
    : action(ext_action)
    , stateID(ext_stateID)
{
}