#include "MenuState.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "../gui/Button.hpp"

MenuState::MenuState(StateManager& stack, Context context)
    : State(stack, context)
    , m_backgroundSprite(context.textures->get("title_screen"))
    , m_guiContainer()
{
    auto playButton = std::make_shared<GUI::Button>(context);
    float positionButtonX = (static_cast<float>(context.window->getSize().x) / 2.f) - 100.f;
    playButton->setPosition(positionButtonX, 350);
    playButton->setText("Play");
    playButton->setCallback([this] ()
    {
        requestStackPop();
        requestStackPush(States::ID::Game);
    });

    auto settingsButton = std::make_shared<GUI::Button>(context);
    settingsButton->setPosition(positionButtonX, 400);
    settingsButton->setText("Settings");
    settingsButton->setCallback([this] ()
    {
        // requestStackPush(States::Settings);
    });

    auto exitButton = std::make_shared<GUI::Button>(context);
    exitButton->setPosition(positionButtonX, 450);
    exitButton->setText("Exit");
    exitButton->setCallback([this] ()
    {
        requestStackPop();
    });

    m_guiContainer.pack(playButton);
    m_guiContainer.pack(settingsButton);
    m_guiContainer.pack(exitButton);
}

void MenuState::draw()
{
    sf::RenderTarget& window = *getContext().window;

    window.setView(window.getDefaultView());
    window.draw(m_backgroundSprite);
    window.draw(m_guiContainer);
}

bool MenuState::update(sf::Time)
{
    sf::RenderTarget* window = getContext().window;
    sf::RenderWindow* w = dynamic_cast<sf::RenderWindow*>(window);
    sf::Vector2i mPos = sf::Mouse::getPosition(*w);

    m_guiContainer.update(mPos);

    return true;
}

bool MenuState::handleEvent(const sf::Event& event)
{
    m_guiContainer.handleEvent(event);
    return false;
}
