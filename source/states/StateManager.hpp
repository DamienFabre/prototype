#ifndef STATEMANEGER_HPP_INCLUDED
#define STATEMANEGER_HPP_INCLUDED 

#include <vector>
#include <map>
#include <functional>
#include <utility>

#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>

#include "State.hpp"
#include "StateIdentifiers.hpp"
#include "../utility/NonCopyable.hpp"
// #include "../resources/ResourceIdentifiers.hpp"

namespace sf
{
    class Event;
    class RenderWindow;
}

class StateManager : private NonCopyable
{
public:

    enum Action { Push, Pop, Clear };

    explicit StateManager(State::Context context);

    template <typename T>
    void registerStates(States::ID stateID);

    void update(sf::Time dt);
    void draw();
    void handleEvent(const sf::Event& event);

    void pushState(States::ID stateID);
    void popState();
    void clearStates();

    bool isEmpty() const;

private:

    State::Ptr createState(States::ID stateID);
    void applyPendingChanges();

    struct PendingChange
    {
        explicit PendingChange(Action ext_action, States::ID ext_stateID = States::ID::None);

        Action action;
        States::ID stateID;
    };

    std::vector<State::Ptr> m_stack;
    std::vector<PendingChange> m_pendingList;
    State::Context m_context;
    std::map<States::ID, std::function<State::Ptr()>> m_factories;
};

template <typename T>
void StateManager::registerStates(States::ID stateID)
{
    m_factories[stateID] = [this] ()
    {
        return State::Ptr(new T(*this, m_context));
    };
}

#endif // STATEMANEGER_HPP_INCLUDED