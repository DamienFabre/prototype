#ifndef STATE_HPP_INCLUDED
#define STATE_HPP_INCLUDED

#include <memory>

#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>

#include "StateIdentifiers.hpp"
#include "../resources/ResourceIdentifiers.hpp"
#include "../lua/LuaContext.hpp"

namespace sf
{
    class RenderTarget;
}

class StateManager;
class Player;
class MusicPlayer;
class SoundPlayer;

class State
{
public:

    using Ptr = std::unique_ptr<State>;

    struct Context
    {
        Context(sf::RenderTarget& ext_window, TextureHolder& ext_textures, 
                FontHolder& ext_fonts, Player& ext_player, MusicPlayer& ext_music, 
                SoundPlayer& ext_sounds, LuaContext& ext_luaContext);

        sf::RenderTarget*   window;
        TextureHolder*      textures;
        FontHolder*         fonts;
        Player*             player;
        MusicPlayer*        music;
        SoundPlayer*        sounds;
        LuaContext*         luaContext;
    };

    State(StateManager& stack, Context context);
    virtual ~State() = default;

    virtual void draw() = 0;
    virtual bool update(sf::Time dt) = 0;
    virtual bool handleEvent(const sf::Event& event) = 0;

protected:

    void requestStackPush(States::ID stateID);
    void requestStackPop();
    void requestStateClear();

    Context getContext() const;

private:

    StateManager* m_stack;
    Context m_context;
};

#endif // STATE_HPP_INCLUDED