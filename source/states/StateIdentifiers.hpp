#ifndef STATEINDENTIFIERS_HPP_INCLUDED
#define STATEINDENTIFIERS_HPP_INCLUDED

namespace States
{
    enum class ID 
    {  
        None,
        Intro,
        Title,
        Game,
        Pause,
        Menu
    };
}

#endif // STATEINDENTIFIERS_HPP_INCLUDED