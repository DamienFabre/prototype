#include "LuaContext.hpp"

#include <iostream>

#include <SFML/Graphics/Sprite.hpp>

#include "../map/Map.hpp"
#include "../utility/Logger.hpp"

const std::string LuaContext::mapModuleName = "map";

void LuaContext::registerMapModule()
{
    static const luaL_Reg functions[] = {
        { "add_sprite", map_add_sprite },
        { "reset", map_reset },
        { nullptr, nullptr }
    };
    registerFunctions(mapModuleName, functions);
    LOG_INFO("Lua API : Module map enregistrer");
}

int LuaContext::map_add_sprite(lua_State *l)
{
    const std::string sprite_id = luaL_checkstring(l, 1);
    const float x = static_cast<float>(luaL_checknumber(l, 2));
    const float y = static_cast<float>(luaL_checknumber(l, 3));

    lua_getglobal(l, "_udata");
    lua_getfield(l, -1, "map");
    Map* map = checkMap(l, -1);

    lua_getfield(l, -2, "texture_holder");
    TextureHolder* th = checkTextureHolder(l, -1);

    sf::Sprite sprite(th->get(sprite_id));
    sprite.setPosition(x, y);

    map->addSprite(sprite);

    return 0;
}

int LuaContext::map_reset(lua_State *l)
{
    lua_getglobal(l, "_udata");
    lua_getfield(l, -1, "map");
    Map* map = checkMap(l, -1);
    map->resetMap();

    return 0;
}