#include "LuaContext.hpp"

#include <iostream>
#include <sstream>
#include <cstdlib>
#include <cctype>

#include "../Application.hpp"

#include "../ecs/ECSIdentifiers.hpp"
#include "../ecs/EntityManager.hpp"
#include "../ecs/IEntity.hpp"
#include "../map/Map.hpp"
#include "../Avatar.hpp"

// std::map<LuaContext*, lua_State*> LuaContext::luaContexts;
LuaContext* LuaContext::currentInstance = nullptr;

/**
 * Destructeur
 */
LuaContext::~LuaContext()
{
    // const std::string str = "for n in pairs(_G) do print(n) end";
    // luaL_dostring(baseState, str.c_str());
    lua_close(baseState);
}

LuaContext* LuaContext::getInstance()
{
    return currentInstance;
}

/**
 * Initialise la classe, création du state lua et ouverture
 * des librairies
 */
void LuaContext::init()
{
    currentInstance = this;

    this->baseState = luaL_newstate();
    luaL_openlibs(baseState);

    // luaContexts[this] = baseState;

    exportEnumToLua(System::enumMap);

    // lua_newtable(baseState);
    // lua_setglobal(baseState, "api");

    // lua_getglobal(baseState, "api");
    // lua_pushstring(baseState, "udata");
    // lua_newtable(baseState);
    // lua_settable(baseState, -3);
    // lua_pop(baseState, 1);
    
    lua_newtable(baseState);
    lua_setglobal(baseState, "_udata");

    // lua_newtable(baseState);
    // lua_setglobal(baseState, "scripted_entity");

    registerModules();

    doScript("scripts/lua_init.lua");
}

void LuaContext::cleanStack(lua_State *l)
{
    lua_settop(l, 0);
}

void LuaContext::collectGarbage()
{
    lua_gc(baseState, LUA_GCCOLLECT, 0);
}

lua_State* LuaContext::getLuaVM() const
{
    return baseState;
}

/**
 * Exécute le script donné
 * @param filename std::string nom du fichier
 */
void LuaContext::doScript(const std::string& filename)
{
    luaL_dofile(baseState, filename.c_str());
}

/**
 * Ajoute une pointeur dans la table global api.udata
 * @param name std::string nom du pointeur
 * @param ptr  void* pointeur
 */
void LuaContext::addUserData(const std::string& name, void* ptr)
{
    lua_getglobal(baseState, "_udata");
    pushPointer(baseState, ptr);
    lua_setfield(baseState, -2, name.c_str());
    lua_pop(baseState, 1);
}

void LuaContext::exportEnumToLua(const std::map<unsigned int, std::string>& enumMap)
{
    for (const auto& pair : enumMap)
    {
        lua_pushnumber(baseState, pair.first);
        lua_setglobal(baseState, pair.second.c_str());
        // std::cout << pair.first << " , " << pair.second << std::endl;
    }
}

/**
 * Checks that a table field is a string and returns it.
 * This function acts like lua_getfield() followed by luaL_checkstring().
 * @param l A Lua state.
 * @param table_index Index of a table in the stack.
 * @param key Key of the field to get in that table.
 * @return The wanted field as an string.
 */
const std::string LuaContext::checkStringField(lua_State* lvm, int index, const std::string& key)
{
    lua_getfield(lvm, index, key.c_str());
    if (not lua_isstring(lvm, -1)) 
    {
        std::ostringstream oss;
        oss << "Bad field '" << key << "' (string expected, got " 
            << luaL_typename(lvm, -1) << ")";
        argError(lvm, index, oss.str());
    }

    const std::string& value = lua_tostring(lvm, -1);
    lua_pop(lvm, 1);
    return value;
}

bool LuaContext::callFunction(const std::string& scriptName, const std::string& funcName)
{
    lookupFunc(scriptName, funcName);
    return doCall(0);
}

bool LuaContext::callFunction(const std::string& scriptName, const std::string& funcName, void *ptr)
{
    lookupFunc(scriptName, funcName);
    pushPointer(baseState, ptr);
    return doCall(1);
}

bool LuaContext::doCall(int nbArg, int nbResults)
{
    if (lua_pcall(baseState, nbArg, nbResults, 0) != LUA_OK)
    {
        LOG_ERR(lua_tostring(baseState, -1));
        lua_pop(baseState, 1);
        return false;
    }
    else
    {
        return true;
    }
}

void LuaContext::lookupFunc(const std::string& scriptName, const std::string& funcName)
{
    lua_getglobal(baseState, "scripted_entity");
    lua_getfield(baseState, -1, scriptName.c_str());
    lua_remove(baseState, -2);
    lua_getfield(baseState, -1, funcName.c_str());
    lua_remove(baseState, -2);
}

/**
 * Affiche l'état du stack lua
 */
void LuaContext::stackDump(lua_State *lvm)
{
    int top = lua_gettop(lvm);
    std::cout << "\nLua Stack Dump:\n" << "  Sizeof stack: " << top << "\n";
    for (int i = top; i >= 1; --i)
    {
        int tp = lua_type(lvm, i);
        switch (tp)
        {
            case LUA_TSTRING:
                std::cout << "  " << lua_tostring(lvm, i) << "\n";
                break;

            case LUA_TBOOLEAN:
                std::cout << "  " << (lua_toboolean(lvm, i) ? "true" : "false") << "\n";
                break;

            case LUA_TNUMBER:
                std::cout << "  " << lua_tonumber(lvm, i) << "\n";
                break;
        
            default:
                std::cout << "  " << lua_typename(lvm, tp) << "\n";
                break;
        }
    }
    std::cout << "  --> End stack dump." << std::endl;
}

/**
 * Like luaL_argerror() but with an std::string message.
 * @param l A Lua state.
 * @param arg_index Index of an argument in the stack.
 * @param message Error message.
 * @return This function never returns.
 */
int LuaContext::argError(lua_State* lvm, int arg_index, const std::string& message) 
{
    return luaL_argerror(lvm, arg_index, message.c_str());
}

/**
 * Met un pointeur dur le stack
 * @param lvm lua_State ou mettre le pointeur
 * @param ptr pointeur
 */
void LuaContext::pushPointer(lua_State *lvm, void* ptr)
{
    if (ptr != nullptr)
        lua_pushlightuserdata(lvm, ptr);
    else
        lua_pushnumber(lvm, 0);
}

/**
 * Vérifie que la variable donné est bien un Application* et la retourne caster
 * @param  l     lua state
 * @param  index index de l'argument sur le stack
 * @return       Application* 
 */
Application* LuaContext::checkApplication(lua_State *l, int index)
{
    Application* app = static_cast<Application*>(lua_touserdata(l, index));
    if (not app) 
        LOG_ERR("checkApplication Invalid pointeur");
    return app;
}

EntityManager* LuaContext::checkEntityManager(lua_State *l, int index)
{
    EntityManager* e_mgr = static_cast<EntityManager*>(lua_touserdata(l, index));
    if (not e_mgr)
        LOG_ERR("checkEntityManager Invalid pointeur");
    return e_mgr;
}

IEntity* LuaContext::checkEntity(lua_State *l, int index)
{
    IEntity* e = static_cast<IEntity*>(lua_touserdata(l, index));
    if (not e)
        LOG_ERR("checkEntity Invalid pointeur");
    return e;
}

TextureHolder* LuaContext::checkTextureHolder(lua_State *l, int index)
{
    TextureHolder* th = static_cast<TextureHolder*>(lua_touserdata(l, index));
    if (not th)
        LOG_ERR("checkTextureHolder Invalid pointeur");
    return th;
}

Map* LuaContext::checkMap(lua_State *l, int index)
{
    Map* map = static_cast<Map*>(lua_touserdata(l, index));
    if (not map)
        LOG_ERR("checkMap Invalid pointeur");
    return map;
}

Avatar* LuaContext::checkAvatar(lua_State *l, int index)
{
    Avatar* avatar = static_cast<Avatar*>(lua_touserdata(l, index));
    if (not avatar)
        LOG_ERR("checkAvatar Invalid pointeur");
    return avatar;
}

/**
 * Enregistre plusieurs fonction dans la table globale "api"
 * @param name      std::string nom de la sous table contenant les fonctions
 * @param functions luaL_Reg listes des fonctions
 */
void LuaContext::registerFunctions(const std::string& name, const luaL_Reg* functions)
{
    lua_newtable(baseState);
    lua_setglobal(baseState, name.c_str());
    lua_getglobal(baseState, name.c_str());
    luaL_setfuncs(baseState, functions, 0);
    lua_pop(baseState, 1);
}

/**
 * Enregistre tous les modules dans lua_State
 */
void LuaContext::registerModules()
{
    registerApplicationModule();
    registerEntityModule();
    registerMapModule();
    registerAvatarModule();
    registerParticleModule();
}

void LuaContext::on_shoot(IEntity* ent)
{
    if (getOnStack("on_shoot")) 
    {
        pushPointer(baseState, ent);
        // pushAvatar(static_cast<Avatar*>(ent));
        doCall(1, 0);
    }
}

bool LuaContext::getOnStack(const std::string& funcName)
{
    bool result = true;
    lua_getglobal(baseState, funcName.c_str());
    if (lua_isfunction(baseState, -1) != 1)
    {
        LOG_ERR("La méthode " + funcName + " n'existe pas !");
        result = false;
    }
    return result;
}

// void LuaContext::pushAvatar(Avatar* avatar)
// {
//     pushPointer(baseState, avatar);
//     luaL_getmetatable(baseState, "luaL_avatar");
//     lua_setmetatable(baseState, -2);
// }