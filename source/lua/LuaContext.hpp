#ifndef LUACONTEXT_HPP_INCLUDED
#define LUACONTEXT_HPP_INCLUDED

#include <string>
#include <map>

#include <lua.hpp>

#include "../resources/ResourceHolder.hpp"
#include "../resources/ResourceIdentifiers.hpp"
#include "../utility/Logger.hpp"

// forward declaration
class Application;
class EntityManager;
class IEntity;
class Map;
class Avatar;

class LuaContext
{
public:

    static const std::string applicationModuleName;
    static const std::string ECSModuleName;
    static const std::string mapModuleName;
    static const std::string avatarModuleName;
    static const std::string particuleModuleName;

    LuaContext() = default;
    ~LuaContext();

    static LuaContext* getInstance();

    void init();
    void cleanStack(lua_State *l);
    void collectGarbage();

    lua_State* getLuaVM() const;

    void doScript(const std::string& filename);
    void addUserData(const std::string& name, void* ptr);
    void exportEnumToLua(const std::map<unsigned int, std::string>& enumMap);

    // void pushAvatar(Avatar* avatar);

    // events
    void on_shoot(IEntity* ent);

    static const std::string checkStringField(lua_State* lvm, int index, const std::string& key);
    
    bool callFunction(const std::string& scriptName, const std::string& funcName);
    bool callFunction(const std::string& scriptName, const std::string& funcName, void *ptr);

    typedef int (FunctionExportedToLua) (lua_State* l);

    static FunctionExportedToLua
        app_load_font,
        app_take_screenshot,
        app_get_mouse_position,

        avatar_get_position,

        entity_create,
        entity_set_sprite,
        entity_set_system,
        entity_set_position,
        entity_set_velocity,
        entity_set_speed,
        entity_set_script,
        entity_get_position,
        entity_get_speed,
        entity_get_velocity,
        entity_set_lifetime,

        map_add_sprite,
        map_reset, 

        particle_set_emitter;

    static void stackDump(lua_State *lvm);

    static void pushPointer(lua_State *lvm, void* ptr);

private:

    bool doCall(int nbArg, int nbResults = 0);
    void lookupFunc(const std::string& scriptName, const std::string& funcName);

    bool getOnStack(const std::string& funcName);

    static int argError(lua_State* lvm, int arg_index, const std::string& message);

    static Application* checkApplication(lua_State *l, int index);
    static EntityManager* checkEntityManager(lua_State *l, int index);
    static IEntity* checkEntity(lua_State *l, int index);
    static TextureHolder* checkTextureHolder(lua_State *l, int index);
    static Map* checkMap(lua_State *l, int index);
    static Avatar* checkAvatar(lua_State *l, int index);

    void registerFunctions(const std::string& name, const luaL_Reg* functions);
    void registerModules();
    void registerApplicationModule();
    void registerEntityModule();
    void registerMapModule();
    void registerAvatarModule();
    void registerParticleModule();

    lua_State *baseState = nullptr;
    static LuaContext* currentInstance;
    // static std::map<LuaContext*, lua_State*> luaContexts;
};

#endif // LUACONTEXT_HPP_INCLUDED