#include "LuaContext.hpp"

#include <iostream>

#include "../Application.hpp"

const std::string LuaContext::applicationModuleName = "app";

void LuaContext::registerApplicationModule()
{
    static const luaL_Reg functions[] = {
        { "test", app_load_font },
        { "take_screenshot", app_take_screenshot },
        { "get_mouse_position", app_get_mouse_position },
        { nullptr, nullptr }
    };
    registerFunctions(applicationModuleName, functions);
    LOG_INFO("Lua API : Module application enregistrer");
}

int LuaContext::app_load_font(lua_State*)
{
    std::cout << "Ceci fonctionne YOUPI !!!" << std::endl;
    return 0;
}

int LuaContext::app_take_screenshot(lua_State *l)
{
    lua_getglobal(l, "_udata");
    lua_getfield(l, -1, "application");

    Application* app = checkApplication(l, -1);
    app->takeScreenshot();

    return 0;
}

int LuaContext::app_get_mouse_position(lua_State *l)
{
    lua_getglobal(l, "_udata");
    lua_getfield(l, -1, "application");

    Application* app = checkApplication(l, -1);
    sf::Vector2f pos = app->getMousePosition();

    lua_pushnumber(l, pos.x);
    lua_pushnumber(l, pos.y);

    return 2;
}