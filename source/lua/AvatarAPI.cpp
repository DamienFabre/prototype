#include "LuaContext.hpp"

#include "../Avatar.hpp"
#include "../utility/Logger.hpp"

const std::string LuaContext::avatarModuleName = "avatar";

void LuaContext::registerAvatarModule() 
{
    static const luaL_Reg functions[] = {
        { "get_position", avatar_get_position },
        { nullptr, nullptr }
    };
    registerFunctions(avatarModuleName, functions);
    
    luaL_newmetatable(baseState, "luaL_avatar");
    luaL_setfuncs(baseState, functions, 0);
    lua_pushvalue(baseState, -1);
    lua_setfield(baseState, -1, "__index");
    lua_setglobal(baseState, "Foo");

    LOG_INFO("Lua API : Module avatar enregistrer");
}

int LuaContext::avatar_get_position(lua_State *l)
{
    Avatar* avatar = checkAvatar(l, 1);
    sf::Vector2f pos(avatar->getPosition());

    lua_pushnumber(l, pos.x);
    lua_pushnumber(l, pos.y);

    return 2;
}