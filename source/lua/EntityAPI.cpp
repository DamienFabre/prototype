#include "LuaContext.hpp"

#include <iostream>

#include "../ecs/IEntity.hpp"
#include "../ecs/EntityManager.hpp"
#include "../ecs/ComponentList.hpp"
#include "../ecs/ComponentFactory.hpp"
#include "../ecs/ECSIdentifiers.hpp"

#include "../utility/Logger.hpp"

const std::string LuaContext::ECSModuleName = "entity";

void LuaContext::registerEntityModule()
{
    static const luaL_Reg functions[] = {
        { "create", entity_create },
        { "set_sprite", entity_set_sprite },
        { "set_system", entity_set_system },
        { "set_position", entity_set_position },
        { "set_velocity", entity_set_velocity },
        { "set_speed", entity_set_speed },
        { "set_script", entity_set_script },
        { "get_position", entity_get_position },
        { "get_speed", entity_get_speed },
        { "get_velocity", entity_set_velocity },
        { "set_lifetime", entity_set_lifetime },
        { nullptr, nullptr }
    };
    registerFunctions(ECSModuleName, functions);
    LOG_INFO("Lua API : Module entity enregistrer");
}

#define luaFunc(func) int LuaContext::func(lua_State *l)

luaFunc(entity_create)
{
    lua_getglobal(l, "_udata");
    lua_getfield(l, -1, "entity_mgr");

    EntityManager* mgr = checkEntityManager(l, -1);
    IEntity* new_entity = mgr->createEntity();

    pushPointer(l, new_entity);
    return 1;
}

luaFunc(entity_set_sprite)
{
    IEntity* entity = checkEntity(l, 1);
    std::string id = luaL_checkstring(l, 2);

    lua_getglobal(l, "_udata");
    lua_getfield(l, -1, "texture_holder");

    TextureHolder* th = checkTextureHolder(l, -1);

    if (entity->hasComponent(Component::ID::Sprite))
        entity->getComponent<Sprite>(Component::ID::Sprite)->sprite.setTexture(th->get(id));
    else
        LOG_ERR("entity_set_sprite l'entité ne dispose pas du composant shape");

    return 0;
}

luaFunc(entity_set_system)
{
    IEntity* entity = checkEntity(l, 1);
    unsigned int systems = luaL_checkunsigned(l, 2);

    lua_getglobal(l, "_udata");
    lua_getfield(l, -1, "entity_mgr");

    EntityManager* mgr = checkEntityManager(l, -1);
    mgr->setEntitySystem(entity, systems);

    return 0;
}

luaFunc(entity_set_position)
{
    IEntity* entity = checkEntity(l, 1);
    float x = static_cast<float>(luaL_checknumber(l, 2));
    float y = static_cast<float>(luaL_checknumber(l, 3));

    if (entity->hasComponent(Component::ID::Position))
        entity->getComponent<Position>(Component::ID::Position)->position = {x, y};
    else
        LOG_ERR("entity_set_position l'entité ne dispose pas du composant position");

    return 0;
}

luaFunc(entity_set_velocity)
{
    IEntity* entity = checkEntity(l, 1);
    float x = static_cast<float>(luaL_checknumber(l, 2));
    float y = static_cast<float>(luaL_checknumber(l, 3));

    if (entity->hasComponent(Component::ID::Movement))
        entity->getComponent<Movement>(Component::ID::Movement)->velocity = {x, y};
    else
        LOG_ERR("entity_set_velocity l'entité ne dispose pas du composant movement");

    return 0;
}

luaFunc(entity_set_speed)
{
    IEntity* entity = checkEntity(l, 1);
    float newSpeed = static_cast<float>(luaL_checknumber(l, 2));

    if (entity->hasComponent(Component::ID::Movement))
        entity->getComponent<Movement>(Component::ID::Movement)->speed = newSpeed;
    else
        LOG_ERR("entity_set_speed l'entité ne dispose pas du composant movement");

    return 0;
}

luaFunc(entity_set_script)
{
    IEntity* entity = checkEntity(l, 1);
    const std::string filename = luaL_checkstring(l, 2);

    if (entity->hasComponent(Component::ID::Script))
        entity->getComponent<Script>(Component::ID::Script)->scriptName = filename;
    else
        LOG_ERR("entity_set_script l'entité ne dispose pas du composant script");
    
    return 0;
}

luaFunc(entity_get_position)
{
    IEntity* entity = checkEntity(l, 1);

    sf::Vector2f pos;
    if (entity->hasComponent(Component::ID::Position))
        pos = entity->getComponent<Position>(Component::ID::Position)->position;
    else
        LOG_ERR("entity_get_position l'entité ne dispose pas du composant position");

    lua_pushnumber(l, pos.x);
    lua_pushnumber(l, pos.y);
    return 2;
}

luaFunc(entity_get_speed)
{
    IEntity* entity = checkEntity(l, 1);

    float speed;
    if (entity->hasComponent(Component::ID::Movement))
        speed = entity->getComponent<Movement>(Component::ID::Movement)->speed;
    else
        LOG_ERR("entity_get_speed l'entité ne dispose pas du composant movement");

    lua_pushnumber(l, speed);
    return 1;
}

luaFunc(entity_get_velocity)
{
    IEntity* entity = checkEntity(l, 1);

    sf::Vector2f velocity;
    if (entity->hasComponent(Component::ID::Movement))
        velocity = entity->getComponent<Movement>(Component::ID::Movement)->velocity;
    else
        LOG_ERR("entity_get_speed l'entité ne dispose pas du composant movement");

    lua_pushnumber(l, velocity.x);
    lua_pushnumber(l, velocity.y);
    return 2;    
}

luaFunc(entity_set_lifetime)
{
    IEntity* entity = checkEntity(l, 1);
    float lt = static_cast<float>(luaL_checknumber(l, 2));

    if (entity->hasComponent(Component::ID::LifeTime))
    {
        entity->getComponent<LifeTime>(Component::ID::LifeTime)->lifeTime = sf::seconds(lt);
        entity->getComponent<LifeTime>(Component::ID::LifeTime)->useIt = true;
    }
    else
        LOG_ERR("entity_set_lifetime l'entité ne dispose pas du composant lifeTime");

    return 0;
}

#undef luaFunc