#ifndef INCLLUDE_COMPONENT_HPP
#define INCLLUDE_COMPONENT_HPP

#include <memory>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/Graphics/Rect.hpp>

#include "../utility/NonCopyable.hpp"

namespace sf
{
    class Event;
}

namespace GUI
{
    class Component : public sf::Drawable, NonCopyable
    {
    public:

        using Ptr = std::shared_ptr<Component>;

        Component() = default;
        virtual ~Component() = default;
    
        virtual bool isSelectable() const = 0;
        bool isSelected() const;

        virtual void select();
        virtual void deselect();

        virtual bool isActive() const;
        virtual void activate();
        virtual void deactivate();

        virtual sf::FloatRect getBoundingRect() const = 0;

        virtual void handleEvent(const sf::Event& event) = 0;

    private:

        bool m_isSelected = false;
        bool m_isActive   = false;
    };
}

#endif // INCLLUDE_COMPONENT_HPP