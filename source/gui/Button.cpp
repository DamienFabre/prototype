#include "Button.hpp"

#include <iostream>

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Window/Event.hpp>

#include "../utility/Utility.hpp"
#include "../resources/SoundPlayer.hpp"

namespace GUI
{
    /**
     * Constructeur
     */
    Button::Button(State::Context context)
        : m_callback()
        , m_sprite(context.textures->get("bouttons"))
        , m_text("", context.fonts->get("main_font"), 16)
        , m_isToggle(false)
        , m_sounds(*context.sounds)
    {
        buttonChangeTexture(Normal);

        // sf::FloatRect bounds = m_sprite.getGlobalBounds();
        // m_text.setPosition(bounds.width / 2.f, bounds.height / 2.f);
    }

    Button::Button(State::Context context, const std::string& text, sf::Vector2f position)
        : m_callback()
        , m_sprite(context.textures->get("bouttons"))
        , m_text("", context.fonts->get("main_font"), 16)
        , m_isToggle(false)
        , m_sounds(*context.sounds)
    {
        buttonChangeTexture(Normal);
        m_sprite.setPosition(position);
        m_text.setString(text);
        centerOrigin(m_text);
        sf::FloatRect bounds = m_sprite.getGlobalBounds();
        m_text.setPosition(bounds.width / 2.f, bounds.height / 2.f);
    }

    /**
     * Definie la fonction de callback
     * @param callback fonction
     */
    void Button::setCallback(Callback callback)
    {
        m_callback = std::move(callback);
    }

    /**
     * Modifie le texte du boutton
     * @param text nouveau texte
     */
    void Button::setText(const std::string& text)
    {
        m_text.setString(text);
        centerOrigin(m_text);
    }

    /**
     * Active ou non le boutton
     * @param flag boolean
     */
    void Button::setToggle(bool flag)
    {
        m_isToggle = flag;
    }

    /**
     * Indique si ce composant est sélectabel
     * @return vrai
     */
    bool Button::isSelectable() const
    {
        return true;
    }

    /**
     * Selectionne ce boutton
     */
    void Button::select()
    {
        Component::select();
        buttonChangeTexture(Selected);

        m_sounds.play("boutton_clic");
    }

    /**
     * Déselectionne ce boutton
     */
    void Button::deselect()
    {
        Component::deselect();
        buttonChangeTexture(Normal);
    }

    /**
     * Active le boutton
     */
    void Button::activate()
    {
        Component::activate();
        if (m_isToggle)
            buttonChangeTexture(Pressed);
        if (m_callback)
            m_callback();
        if (!m_isToggle)
            deactivate();

        m_sounds.play("boutton_clic");
    }

    /**
     * Désactive le boutton
     */
    void Button::deactivate()
    {
        Component::deactivate();
        if (m_isToggle)
        {
            if (isSelected())
                buttonChangeTexture(Selected);
            else
                buttonChangeTexture(Normal);;
        }
    }

    void Button::handleEvent(const sf::Event&)
    {
    }

    /**
     * Déssine le boutton
     * @param target fenetre où déssiner
     * @param states états
     */
    void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        // states.transform *= getTransform();
        target.draw(m_sprite, states);
        target.draw(m_text, states);
    }

    /**
     * Change la texture du boutton
     * @param buttonType indique le type (enfoncé, normal, sélectionné etc...)
     */
    void Button::buttonChangeTexture(Type buttonType)
    {
        sf::IntRect textureRect(0, 50 * buttonType, 200, 50);
        m_sprite.setTextureRect(textureRect);
    }

    /**
     * Retourne la position et la taille du boutton
     * @return FloatRect
     */
    sf::FloatRect Button::getBoundingRect() const
    {
        return m_sprite.getGlobalBounds();
    }

    void Button::setPosition(float x, float y)
    {
        m_sprite.setPosition(x, y);

    }
} // namespace GUI