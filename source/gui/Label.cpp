#include "Label.hpp"

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

namespace GUI
{
    /**
     * Constructeur
     */
    Label::Label(const std::string& text, const FontHolder& fonts)
        : m_text(text, fonts.get("main_font"), 16)
    {
    }

    /**
     * Indique si le label est sélectionnable
     * @return boolean
     */
    bool Label::isSelectable() const
    {
        return false;
    }

    /**
     * Change le texte du label
     * @param text nouveau texte
     */
    void Label::setText(const std::string& text)
    {
        m_text.setString(text);
    }

    void Label::handleEvent(const sf::Event&)
    {
    }

    /**
     * Déssine le label
     * @param target fenetre où déssiner
     * @param states états
     */
    void Label::draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        // states.transform *= getTransform();
        target.draw(m_text, states);
    }

    /**
     * Retourne la position et la taille du label
     * @return FloatRect
     */
    sf::FloatRect Label::getBoundingRect() const
    {
        return m_text.getGlobalBounds();
    }
}