#include "Component.hpp"

namespace GUI
{
    /**
     * indique si le composant est sélectionné
     * @return boolean
     */
    bool Component::isSelected() const
    {
        return m_isSelected;
    }

    /**
     * Sélectionne le composant
     */
    void Component::select()
    {
        m_isSelected = true;
    }

    /**
     * Désélectionne le composant
     */
    void Component::deselect()
    {
        m_isSelected = false;
    }

    /**
     * Indique si le composant est sélectionné
     * @return boolean
     */
    bool Component::isActive() const
    {
        return m_isActive;
    }

    /**
     * Active le composant
     */
    void Component::activate()
    {
        m_isActive = true;
    }

    /**
     * Désactive le composant
     */
    void Component::deactivate()
    {
        m_isActive = false;
    }
}