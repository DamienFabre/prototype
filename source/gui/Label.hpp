#ifndef INCLUDE_LABLE_HPP
#define INCLUDE_LABLE_HPP

#include <SFML/Graphics/Text.hpp>

#include "Component.hpp"
#include "../resources/ResourceIdentifiers.hpp"
#include "../resources/ResourceHolder.hpp"

namespace GUI
{
    class Label : public Component
    {
    public:

        using Ptr = std::shared_ptr<Label>;

        Label(const std::string& text, const FontHolder& fonts);
        ~Label() = default;

        virtual bool isSelectable() const;
        void setText(const std::string& text);

        virtual sf::FloatRect getBoundingRect() const;

        virtual void handleEvent(const sf::Event& event);

    private:

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        sf::Text m_text;
    };
}

#endif // INCLUDE_LABLE_HPP