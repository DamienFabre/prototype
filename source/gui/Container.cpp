#include "Container.hpp"

#include <iostream>

#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RenderStates.hpp>

namespace GUI
{
    /**
     * Ajout un élément au container
     * @param component élément à grouper
     */
    void Container::pack(Component::Ptr component)
    {
        m_children.push_back(component);
        m_childrenSet.insert(component);
        if (!hasSelection() && component->isSelectable())
        {
            selectComponent(m_children.size() - 1);
            // selectComponent(*m_childrenSet.find(component));
        }
    }

    /**
     * Indique si ce composant est sélectionnable
     * @return boolean
     */
    bool Container::isSelectable() const
    {
        return false;
    }

    /**
     * Gère les déplacements dans le menu et les délégation des évènements au élement actifs
     * @param evenement à traiter
     */
    void Container::handleEvent(const sf::Event& event)
    {
        if (hasSelection() && m_children[m_selectChild]->isActive())
        {
            m_children[m_selectChild]->handleEvent(event);
        }
        else if (event.type == sf::Event::KeyReleased)
        {
            if (event.key.code == sf::Keyboard::Up || event.key.code == sf::Keyboard::Z)
            {
                selectPrevious();
            }
            else if (event.key.code == sf::Keyboard::Down || event.key.code == sf::Keyboard::S)
            {
                selectNext();
            }
            else if (event.key.code == sf::Keyboard::Return || event.key.code == sf::Keyboard::Space)
            {
                if (hasSelection())
                    m_children[m_selectChild]->activate();
            }
        }
        else if (event.type == sf::Event::MouseButtonReleased)
        {
            if (event.mouseButton.button == sf::Mouse::Left)
            {
                std::cout << "yoooo" << std::endl;
            }
        }
    }

    /**
     * Déssine le container
     * @param target fenetre où déssiner
     * @param states états
     */
    void Container::draw(sf::RenderTarget& target, sf::RenderStates states) const
    {
        // states.transform *= getTransform();

        for (const Component::Ptr& child : m_children)
            target.draw(*child, states);
    }

    /**
     * Retourne vrai si un enfant est selectionné
     * @return bool
     */
    bool Container::hasSelection() const
    {
        return m_selectChild >= 0;
    }

    /**
     * Selectionne un composant
     * @param index du composant 
     */
    void Container::selectComponent(std::size_t index)
    {
        if (m_children[index]->isSelectable())
        {
            if (hasSelection())
                m_children[m_selectChild]->deselect();
            m_children[index]->select();
            m_selectChild = index;
        }
    }

    /**
     * selectionne le composant suivant
     */
    void Container::selectNext()
    {
        if (!hasSelection())
            return;

        long next = m_selectChild;
        do 
            next = (next + 1) % m_children.size();
        while (!m_children[next]->isSelectable());

        selectComponent(next);
    }

    /**
     * selectionne le composant précédent
     */
    void Container::selectPrevious()
    {
        if (!hasSelection())
            return;

        long prev = m_selectChild;
        do
            prev = (prev + m_children.size() - 1) % m_children.size();
        while (!m_children[prev]->isSelectable());

        selectComponent(prev);
    }

    /**
     * Retourne la position et la taille du container
     * @return FloatRect
     */
    sf::FloatRect Container::getBoundingRect() const
    {
        return sf::FloatRect();
    }

    /**
     * Met à jour le container et ses éléments
     * @param dt temps écoulé
     */
    void Container::update(sf::Vector2i mousePos)
    {
        for (auto& elem : m_children)
        {
            if (elem->isSelectable())
            {
                sf::FloatRect rect = elem->getBoundingRect();
                if (rect.contains(static_cast<sf::Vector2f>(mousePos)))
                {
                    selectComponent(elem);
                }
            }
        }
    }

    void Container::selectComponent(Component::Ptr comp)
    {
        if (m_selectChildSet != nullptr)
            m_selectChildSet->deselect();
        m_selectChildSet = comp;
        m_selectChildSet->select();
    }
}