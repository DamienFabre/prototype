#ifndef INCLUDE_CONTAINER_HPP
#define INCLUDE_CONTAINER_HPP

#include <memory>
#include <vector>
#include <set>

#include "Component.hpp"

namespace GUI
{
    class Container : public Component
    {
    public:

        using Ptr = std::shared_ptr<Container>;

        Container() = default;
        ~Container() = default;
        
        void pack(Component::Ptr component);

        virtual bool isSelectable() const;
        virtual void handleEvent(const sf::Event& event);

        virtual sf::FloatRect getBoundingRect() const;
        void update(sf::Vector2i mousePos);

    private:

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

        bool hasSelection() const;
        void selectComponent(std::size_t index);
        void selectComponent(Component::Ptr comp);
        void selectNext();
        void selectPrevious();

        std::vector<Component::Ptr> m_children       = {};
        std::set<Component::Ptr>    m_childrenSet    = {};
        long                        m_selectChild    = -1;
        Component::Ptr              m_selectChildSet = nullptr;
    
    };
}

#endif // INCLUDE_CONTAINER_HPP