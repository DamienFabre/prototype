#ifndef INCLUDE_BUTTON_HPP
#define INCLUDE_BUTTON_HPP

#include <functional>
#include <vector>
#include <string>
#include <memory>

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "Component.hpp"
#include "../resources/ResourceIdentifiers.hpp"
#include "../resources/ResourceHolder.hpp"
#include "../states/State.hpp"

class SoundPlayer;

namespace GUI
{
    class Button : public Component
    {
    public:

        enum Type
        {
            Normal,
            Selected,
            Pressed
        };

        using Ptr = std::shared_ptr<Button>;
        using Callback = std::function<void()>;

        Button(State::Context context);
        Button(State::Context context, const std::string& text, sf::Vector2f position);
        ~Button() = default;

        void setCallback(Callback callback);
        void setText(const std::string& text);
        void setToggle(bool flag);
        void setPosition(float x, float y);

        virtual bool isSelectable() const;
        virtual void select();
        virtual void deselect();

        virtual void activate();
        virtual void deactivate();

        virtual sf::FloatRect getBoundingRect() const;

        virtual void handleEvent(const sf::Event& event);

    private:

        virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
        void buttonChangeTexture(Type buttonType);

        Callback     m_callback;
        sf::Sprite   m_sprite;
        sf::Text     m_text;
        bool         m_isToggle;
        SoundPlayer& m_sounds;
    };
} // namespace GUI

#endif // INCLUDE_BUTTON_HPP