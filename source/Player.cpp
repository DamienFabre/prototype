#include "Player.hpp"
#include "events/CommandQueue.hpp"
#include "Avatar.hpp"

using namespace std::placeholders;

// struct AvatarMover
// {
//     Mover(float vx, float vy) : velocity(vx, vy) {}

//     void operator() (IEntity* e, sf::Time dt) const
//     {
//         assert(dynamic_cast<Avatar*>(e) != nullptr);
//         Avatar *avatar = static_cast<Avatar*>(e);
//         avatar->move(sf::Vector2f(0.f, 1 * dt.asSeconds()));
//     }

//     sf::Vector2f velocity;
// };

/// Constructeur
Player::Player()
{
    // Set initial key binding
    m_keyBinding[sf::Keyboard::Left] = MoveLeft;
    m_keyBinding[sf::Keyboard::Right] = MoveRight;
    m_keyBinding[sf::Keyboard::Up] = MoveUp;
    m_keyBinding[sf::Keyboard::Down] = MoveDown;
    m_keyBinding[sf::Keyboard::A] = Fire;

    // set initial action binding
    initActions();

    // Assign all category to player Aircraft
    for (auto& pair : m_actionBinding)
        pair.second.category = Category::Player;
}

/**
 * @brief Envoi dans la queue les evenement reçus si il ne font pas partie de ceux en temps réèl
 * @param event évenement à traité
 * @param commands queue où est inseré l'évenement
 */
void Player::handleEvent(const sf::Event& event, CommandQueue& commands)
{
    if (event.type == sf::Event::KeyPressed)
    {
        auto found = m_keyBinding.find(event.key.code);
        if (found != m_keyBinding.end() && !isRealTimeAction(found->second))
            commands.push(m_actionBinding[found->second]);
    }
}

/**
 * @brief Créer une conmmande à chaque frame par rapport au touche (enfoncé ou non)
 * @param commands queue où sont ajouté les nouvelles commandes
 */
void Player::handleRealTimeInput(CommandQueue& commands)
{
    for (auto pair : m_keyBinding)
    {
        if (sf::Keyboard::isKeyPressed(pair.first) && isRealTimeAction(pair.second))
            commands.push(m_actionBinding[pair.second]);
    }
}

void Player::assignKey(Action action, sf::Keyboard::Key key)
{
    for (auto itr = m_keyBinding.begin(); itr != m_keyBinding.end(); )
    {
        if (itr->second == action)
            m_keyBinding.erase(itr++);
        else
            ++itr;
    }

    m_keyBinding[key] = action;
}

sf::Keyboard::Key Player::getAssignedKey(Action action) const
{
    for (auto pair : m_keyBinding)
    {
        if (pair.second == action)
            return pair.first;
    }

    return sf::Keyboard::Unknown;
}

/**
 * @brief Vérifie si une action fait partie des actions en temps réèls
 * @param action action à tester
 * @return Vrai si c'est une action temps réel
 */
bool Player::isRealTimeAction(Action action)
{
    switch (action)
    {
        case MoveLeft:
        case MoveRight:
        case MoveUp:
        case MoveDown:
        case Fire:
            return true;

        case ActionCount:
        default:
            return false;
    }
}

/**
 * @brief Initialise les actions
 */
void Player::initActions()
{
    m_actionBinding[MoveDown].action = [] (void* e, sf::Time dt) {
        Avatar *avatar = static_cast<Avatar*>(e);
        avatar->move(sf::Vector2f(0.f, 1 * dt.asSeconds()));
    };
    
    m_actionBinding[MoveUp].action = [] (void* e, sf::Time dt) {
        Avatar *avatar = static_cast<Avatar*>(e);
        avatar->move(sf::Vector2f(0.f, -1 * dt.asSeconds()));
    };
    
    m_actionBinding[MoveLeft].action = [] (void* e, sf::Time dt) {
        Avatar *avatar = static_cast<Avatar*>(e);
        avatar->move(sf::Vector2f(-1 * dt.asSeconds(), 0.f));
    };
    
    m_actionBinding[MoveRight].action = [] (void* e, sf::Time dt) {
        Avatar *avatar = static_cast<Avatar*>(e);
        avatar->move(sf::Vector2f(1 * dt.asSeconds(), 0.f));
    };

    m_actionBinding[Fire].action = [] (void* e, sf::Time) {
        Avatar *avatar = static_cast<Avatar*>(e);
        avatar->fire();
    };
}

// void Player::setMissionStatus(MissionStatus status)
// {
//     m_currentMissionStatus = status;
// }

// Player::MissionStatus Player::getMissionStauts() const
// {
//     return m_currentMissionStatus;
// }