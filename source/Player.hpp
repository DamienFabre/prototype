#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "events/Command.hpp"

#include <SFML/Window/Event.hpp>

#include <map>

class CommandQueue;

class Player
{
public:

    enum Action
    {
        MoveLeft,
        MoveRight,
        MoveUp,
        MoveDown,
        Fire,
        ActionCount,
    };

    // enum MissionStatus
    // {
    //  MissionRunning,
    //  MissionSuccess,
    //  MissionFailure
    // };

    Player();
    ~Player() = default;

    void handleEvent(const sf::Event& event, CommandQueue& commands);
    void handleRealTimeInput(CommandQueue& commands);

    void assignKey(Action action, sf::Keyboard::Key key);
    sf::Keyboard::Key getAssignedKey(Action action) const;

    // void setMissionStatus(MissionStatus status);
    // MissionStatus getMissionStauts() const;

private:

    static bool isRealTimeAction(Action action);
    void initActions();

    std::map<sf::Keyboard::Key, Action> m_keyBinding;
    std::map<Action, Command>           m_actionBinding;
    // MissionStatus                       m_currentMissionStatus;

};

#endif // PLAYER_HPP