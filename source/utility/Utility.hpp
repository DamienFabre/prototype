#ifndef INCLUDE_UTILITY_HPP
#define INCLUDE_UTILITY_HPP

#include <sstream>
#include <random>
#include <ctime>
#include <algorithm>
#include <limits>

#include <SFML/Window/Keyboard.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Rect.hpp>
// #include <Box2D/Box2D.h>

namespace sf
{
    class Sprite;
    class Text;
}

namespace
{
    std::default_random_engine createRandomEngine()
    {
        auto seed = static_cast<unsigned long>(std::time(nullptr));
        return std::default_random_engine(seed);
    }
    auto RandomEngine = createRandomEngine();
} // namespace anonyme

// some constante
const float pi = 3.141592653589793238462643383f;
const float scale = 3.2f;

/*
// ==== BOX2D Utility ==== //
// convertion mètre en pixel et inversement
inline float toMet(float x) { return x / scale; }
inline float toPix(float x) { return x * scale; }
// convertion des vecteurs
inline sf::Vector2f toVector2f(b2Vec2 vec) { return sf::Vector2f(toPix(vec.x), toPix(vec.y)); }
inline b2Vec2 tob2Vec2(sf::Vector2f vec) { return b2Vec2(toMet(vec.x), toMet(vec.y)); }
*/

// convertion degrée radian
inline float toDeg(float angle) { return (180.f / pi) * angle; }
inline float toRad(float angle) { return (pi / 180.f) * angle; }

inline bool equals(float nb1, float nb2, float precision = 0.0001f)
{
    float epsilon = precision;
    return std::abs(nb1 - nb2) <= epsilon;
}

// Utility for sfml
template <typename T>
void centerOrigin(T& object);

// for Math
float toDegree(float radian);
float toRadian(float degree);
float length(sf::Vector2f vector);
sf::Vector2f unitVector(sf::Vector2f vector);

// Valeur min et max d'un type de variable
template <typename T>
T getMinValue();

template <typename T>
T getMaxValue();

// Random number
template <typename T>
T rand(T max, T min = 0);

#include "Utility.inl"
#endif // INCLUDE_UTILITY_HPP



