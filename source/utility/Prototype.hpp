#ifndef PROTOTYPE_HPP_INCLUDED
#define PROTOTYPE_HPP_INCLUDED

#include <memory>

template <class T>
class Prototype 
{
public:

    Prototype() = default;
    virtual ~Prototype() = default;
    virtual std::unique_ptr<T> clone() const = 0;
};

#endif // PROTOTYPE_HPP_INCLUDED