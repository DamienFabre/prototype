// Utility for sfml
template <typename T>
void centerOrigin(T& object)
{
    sf::FloatRect rect = object.getLocalBounds();
    object.setOrigin(rect.width / 2.f, rect.height / 2.f);
}

// Random number
template <typename T>
T rand(T max, T min)
{
    if (max < min)
    {
        T tmp = max;
        max = min;
        min = tmp;
    }
    if (std::is_integral<T>::value)
    {
        std::uniform_int_distribution<> dist(min, max);
        return static_cast<T>(dist(RandomEngine));
    }
    else // if (std::is_floating_point<T>::value)
    {
        std::uniform_real_distribution<> dist(0, max);
        return static_cast<T>(dist(RandomEngine));
    }
}

template <typename T>
inline T getMinValue()
{
    return std::numeric_limits<T>::lowest();
}

template <typename T>
inline T getMaxValue()
{
    return std::numeric_limits<T>::max();
}