#include "Logger.hpp"

std::fstream Logger::m_logFile("log", std::ios::out | std::ios::app);
unsigned int Logger::m_numWarnings(0u);
unsigned int Logger::m_numErrors(0u);
unsigned int Logger::m_numInfos(0u);

/**
 * Fonction optionnelle qui permet de d'afficher des informations
 * au début du fichier de log
 * @param info phrase à afficher
 */
void Logger::setLogInfo(const std::string& info)
{
	char ltime[20];
    std::time_t t = std::time(nullptr);
    std::strftime(ltime, 20, "%T %x", std::localtime(&t));
	m_logFile << info << "\nLog file created at " 
			  << ltime << "\n" << std::endl;
}

/**
 * Ferme le fichier de log
 */
void Logger::close()
{
	if (m_logFile.is_open())
	{
		m_logFile << "\n" << m_numErrors << " errors\n"
				  << m_numWarnings       << " warnings\n"
				  << m_numInfos          << " infos\n\n" 
				  << "========================================================="
				  << "====================\n" << std::endl;

		m_logFile.close();
	}
}
