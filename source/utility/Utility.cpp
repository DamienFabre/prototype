#include "Utility.hpp"

#include <cassert>

float toDegree(float radian)
{
    return 180.f / pi * radian;
}

float toRadian(float degree)
{
    return pi / 180.f * degree;
}

float length(sf::Vector2f vector)
{
    return std::sqrt((vector.x * vector.x) + (vector.y * vector.y));
}

sf::Vector2f unitVector(sf::Vector2f vector)
{
    if (vector == sf::Vector2f(0.f, 0.f)) return sf::Vector2f();
    return vector / length(vector);
}

