#ifndef NON_COPYABLE_HPP_INCLUDED
#define NON_COPYABLE_HPP_INCLUDED

class NonCopyable 
{
public:

    NonCopyable() = default;

    NonCopyable(const NonCopyable&) = delete;
    NonCopyable& operator= (const NonCopyable&) = delete;
};

#endif // NON_COPYABLE_HPP_INCLUDED