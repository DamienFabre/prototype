#ifndef INCLUDE_LOGGER_HPP
#define INCLUDE_LOGGER_HPP 

#include <string>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <ctime>

#define LOG_INFO(str) { Logger log; log << Logger::LOG_INFO << str; }
#define LOG_WARN(str) { Logger log; log << Logger::LOG_WARNING << str; }
#define LOG_ERR(str) { Logger log; log << Logger::LOG_ERROR << str; }

class Logger
{
public:

    enum e_logType { LOG_ERROR, LOG_WARNING, LOG_INFO };

    Logger() = default;
    ~Logger() = default;

    void setLogInfo(const std::string& info);

    void close();
    
    void Log(const e_logType type, const std::string& text);
    void Log(const std::string& text);

    friend Logger& operator<< (Logger& logger, const e_logType& type)
    {
        char ltime[20];
        std::time_t t = std::time(nullptr);
        std::strftime(ltime, 20, "[%T] ", std::localtime(&t));
        logger.m_logFile << ltime;

        switch (type)
        {
            case e_logType::LOG_WARNING:
                logger.m_logFile << "[WARNING] ";
                ++logger.m_numWarnings;
                break;

            case e_logType::LOG_ERROR:
                logger.m_logFile << "[ERROR]   ";
                ++logger.m_numErrors;
                break;

            case e_logType::LOG_INFO:
                logger.m_logFile << "[INFO]    ";
                ++logger.m_numInfos;
                break;

            default:
                break;
        }
        return logger;
    }

    friend Logger& operator<< (Logger& logger, const std::string& text)
    {
        logger.m_logFile << text << std::endl;
        return logger;
    }

private:

    static std::fstream m_logFile;

    static unsigned int m_numWarnings;
    static unsigned int m_numErrors;
    static unsigned int m_numInfos;
};

#endif // INCLUDE_LOGGER_HPP