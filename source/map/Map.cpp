#include "Map.hpp"

void Map::resetMap()
{
    m_map.clear();
    m_spriteMap.clear();
}

void Map::addTile(const Tile& tile)
{
    m_map.emplace_back(tile);
}

void Map::removeTile(const Tile& )
{

}

const std::vector<Map::Tile>& Map::getMap() const
{
    return m_map;
}

void Map::addSprite(sf::Sprite spr)
{
    m_spriteMap.push_back(spr);
}

void Map::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    // for (auto& tile : m_map)
    // {
    //     tile.sprite.setPosition(tile.coord.x * tile.size.x, tile.coord.y * tile.size.y);
    //     target.draw(tile.sprite, states);
    // }
    for (const auto& sprite : m_spriteMap)
    {
        target.draw(sprite, states);
    }
}
