#ifndef MAP_HPP_INCLUDED
#define MAP_HPP_INCLUDED

#include <vector>
#include <string>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics.hpp>

class Map : public sf::Drawable
{
public:

    struct Tile
    {
        std::string  sprite_id;
        sf::Sprite   sprite;
        sf::Vector2u coord;
        sf::Vector2u size;
    };

    Map() = default;
    ~Map() = default;

    void resetMap();
    void addTile(const Tile& tile);
    void removeTile(const Tile& tile);
    const std::vector<Tile>& getMap() const;

    void addSprite(sf::Sprite spr);

private:

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

    std::vector<Tile> m_map = {};

    std::vector<sf::Sprite> m_spriteMap = {};
};

#endif // MAP_HPP_INCLUDED