#include "CommandQueue.hpp"

std::queue<Command> CommandQueue::m_queue = {};

void CommandQueue::push(const Command& command)
{
    m_queue.push(command);
}

Command CommandQueue::pop()
{
    Command result = m_queue.front();
    m_queue.pop();
    return result;
}

bool CommandQueue::isEmpty() const
{
    return m_queue.empty();
}
