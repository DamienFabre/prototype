#ifndef COMMANDQUEUE_HPP_INCLUDED
#define COMMANDQUEUE_HPP_INCLUDED

#include <queue>

#include "Command.hpp"

class CommandQueue
{
public:

    CommandQueue() = default;
    ~CommandQueue() = default;

    static void push(const Command& command);
    Command pop();
    bool isEmpty() const;

private:

    static std::queue<Command> m_queue;
};

#endif // COMMANDQUEUE_HPP_INCLUDED