#ifndef COMMAND_HPP_INCLUDED
#define COMMAND_HPP_INCLUDED

#include <functional>
#include <cassert>

#include <SFML/System/Time.hpp>

#include "Category.hpp"
#include "../ecs/IEntity.hpp"

struct Command
{
    Command() : action(), category(Category::None) {}
    Command(std::function<void(void *, sf::Time)> act, unsigned int cat)
        : action(act), category(cat) {}

    std::function<void(void *, sf::Time)> action;
    unsigned int category;
};

// template <typename GameObject, typename Function>
// std::function<void(void*, sf::Time)> derivedAction(Function fn)
// {
//     return [=] (void* node, sf::Time dt)
//     {
//         // Check if is cast safe
//         assert(dynamic_cast<GameObject*>(&node) != nullptr);

//         // Downcast node and invoke function on it
//         fn(static_cast<GameObject&>(node), dt);
//     };
// }

#endif // COMMAND_HPP_INCLUDED