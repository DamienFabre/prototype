#ifndef CATEGORY_HPP_INCLUDED
#define CATEGORY_HPP_INCLUDED

// on énumère ici chaque type de noeud 

namespace Category
{
    enum Type
    {
        None   = 0,
        Type1  = 1 << 0,
        Type2  = 1 << 1,
        Player = 1 << 2,
        Entity = 1 << 3, 
        System  = 1 << 4
    };
}


#endif // CATEGORY_HPP_INCLUDED