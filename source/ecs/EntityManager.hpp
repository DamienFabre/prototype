#ifndef ENTITY_MANAGER_HPP_INCLUDED
#define ENTITY_MANAGER_HPP_INCLUDED

#include <map>
#include <cassert>

#include <SFML/System/Time.hpp>

#include "ECSIdentifiers.hpp"
#include "IEntity.hpp"
#include "../utility/NonCopyable.hpp"
#include "../events/Category.hpp"
#include "../events/Command.hpp"

// forward declaration
class SystemManager;

class EntityManager : private NonCopyable
{
public:

    explicit EntityManager(SystemManager& sys);
    ~EntityManager() = default;

    IEntity* createEntity();
    // void setupEntity(IEntity *entity, Category::Type category, unsigned int systems,
    //                  const std::string& spriteName, const std::string& scriptName);
    void setEntitySystem(IEntity* entity, unsigned int sys);
    void removeEntity(unsigned int id);
    void clearAllEntity();

    std::vector<IEntity*> getEntityList() const;
    IEntity& getEntity(const unsigned int id) const;
    IEntity& getPlayer() const;
    unsigned int getNbEntiy() const;
    unsigned int getPlayerID() const;

    void update(sf::Time dt);
    void onCommand(const Command& command, sf::Time dt);

private:

    std::map<unsigned int, IEntity::Ptr> m_entities;
    unsigned int                         m_nextEntityID;
    SystemManager&                       m_systems;
    unsigned int                         m_playerID;
    unsigned int                         m_category;
}; 

#endif // ENTITY_MANAGER_HPP_INCLUDED