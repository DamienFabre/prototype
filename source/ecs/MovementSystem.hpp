#ifndef MOVEMENT_SYSTEM_HPP_INCLUDED
#define MOVEMENT_SYSTEM_HPP_INCLUDED

#include "ISystem.hpp"
#include "ECSIdentifiers.hpp"

class MovementSystem : public ISystem
{
public:

    MovementSystem(System::ID id);

    virtual void init() override;
    virtual void update(sf::Time dt, std::vector<IEntity*>& entities) override;
    virtual void setupEntity(IEntity& entity, ComponentFactory& factory) override;
};

#endif // MOVEMENT_SYSTEM_HPP_INCLUDED