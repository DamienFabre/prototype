#ifndef SYSTEM_MAANAGER_HPP_INCLUDED
#define SYSTEM_MAANAGER_HPP_INCLUDED

#include <map>
#include <vector>
#include <cassert>

#include "ECSIdentifiers.hpp"
#include "ISystem.hpp"
#include "../utility/NonCopyable.hpp"

// forward declaration
class ComponentFactory;
class IEntity;

class SystemManager : private NonCopyable
{
public:

    explicit SystemManager(ComponentFactory& factory);
    ~SystemManager() = default;

    void init();

    void removeSystem(System::ID id);
    void update(sf::Time dt, std::vector<IEntity*>& entities);

    // template <typename T>
    void setupEntity(System::ID id, IEntity& entity);
    void setupEntity(IEntity& entity, unsigned int systems);
    bool isEmpty();

private:

    template <typename T>
    void addSystem(System::ID id);

    std::map<System::ID, ISystem::Ptr> m_systems;
    ComponentFactory& m_compFactory;
};

/**
 * Ajoute un système
 * @param id     id du system
 * @param system pointeur sur le system
 */
template <typename T> 
void SystemManager::addSystem(System::ID id)
{
    auto found = m_systems.find(id);
    assert(found == m_systems.end());
    m_systems.insert(std::make_pair(id, ISystem::Ptr(new T(id))));
}

#endif // SYSTEM_MAANAGER_HPP_INCLUDED