#ifndef COMPONENT_FACTORY_HPP_INCLUDED
#define COMPONENT_FACTORY_HPP_INCLUDED

#include <map>
#include <memory>
#include <functional>

#include "ECSIdentifiers.hpp"
#include "IComponent.hpp"
#include "../utility/NonCopyable.hpp"
#include "ComponentList.hpp"

class ComponentFactory : private NonCopyable
{
public:

    explicit ComponentFactory() = default;

    void init();

    template<typename T>
    void registerComponent(const Component_ID id);

    IComponent::Ptr getComponent(const Component_ID id);
    std::size_t getNbComponent() const;

private:

    std::map<Component_ID, std::function<IComponent::Ptr()>> m_factory = {};
};

/**
 * Ajoute un nouveaux composant à la factory
 * @param Component::ID Id du composant
 * @param comp          pointeur sur le composant
 */
template<typename T>
void ComponentFactory::registerComponent(const Component_ID id)
{
    // si le composant n'est pas déja enregistrer
    if (m_factory.find(id) == m_factory.end())
        m_factory[id] = [=] () { return IComponent::Ptr(new T(id)); };
}

#endif // COMPONENT_FACTORY_HPP_INCLUDED