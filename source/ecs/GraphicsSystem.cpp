#include "GraphicsSystem.hpp"

/**
 * Constructeur
 */
GraphicsSystem::GraphicsSystem(System::ID id) : ISystem(id)
{
}

/**
 * Initialise le systeme
 */
void GraphicsSystem::init()
{
}

/**
 * Met à jour tous les systemes enregistrer
 * @param dt       temps écoulé
 * @param entities tableau d'entité à mettre à jour
 */
void GraphicsSystem::update(sf::Time , std::vector<IEntity*>& )
{
}

/**
 * Ajoute les composant nécéssaire à l'entité pour pouvoir être utilisée 
 * par ce système
 * @param entity à monter
 */
void GraphicsSystem::setupEntity(IEntity& entity, ComponentFactory& factory)
{
    entity.addComponent(factory.getComponent(Component::ID::Shape));
    entity.addComponent(factory.getComponent(Component::ID::Sprite));
}
