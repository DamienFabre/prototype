#ifndef GRAPHICS_SYSTEM_HPP_INCLUDED
#define GRAPHICS_SYSTEM_HPP_INCLUDED

#include "ISystem.hpp"
#include "ECSIdentifiers.hpp"

class GraphicsSystem : public ISystem
{
public:

    GraphicsSystem(System::ID id);

    virtual void init();
    virtual void update(sf::Time dt, std::vector<IEntity*>& entities);
    virtual void setupEntity(IEntity& entity, ComponentFactory& factory);
};

#endif // GRAPHICS_SYSTEM_HPP_INCLUDED