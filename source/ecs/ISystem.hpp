#ifndef I_SYSTEM_HPP_INCLUDED
#define I_SYSTEM_HPP_INCLUDED

#include <memory>
#include <vector>
#include <functional>

#include <SFML/System/Time.hpp>

#include "IEntity.hpp"
#include "ComponentFactory.hpp"

class ISystem 
{
public:

    using Ptr = std::unique_ptr<ISystem>;

    ISystem(System::ID id) : m_typeID(id) {}
    virtual ~ISystem() = default;

    virtual void init() = 0;
    virtual void update(sf::Time dt, std::vector<IEntity*>& entities) = 0;

    virtual void setupEntity(IEntity& entity, ComponentFactory& factory) = 0;

private:

    System::ID m_typeID;
};

#endif // I_SYSTEM_HPP_INCLUDED