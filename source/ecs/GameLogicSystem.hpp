#ifndef GAME_LOGIC_SYSTEM_HPP_INCLUDED
#define GAME_LOGIC_SYSTEM_HPP_INCLUDED

#include "ISystem.hpp"
#include "ECSIdentifiers.hpp"

class LuaContext;

class GameLogicSystem : public ISystem 
{
public:

    GameLogicSystem(System::ID id);

    virtual void init() override final;
    virtual void update(sf::Time dt, std::vector<IEntity*>& entities) override final;
    virtual void setupEntity(IEntity& entity, ComponentFactory& factory) override final;

private:

    LuaContext& m_lua;
};

#endif // GAME_LOGIC_SYSTEM_HPP_INCLUDED