#include "EntityManager.hpp"

#include <cassert>
#include <algorithm>

#include "SystemManager.hpp"

/**
 * Constructeur
 */
EntityManager::EntityManager(SystemManager& sys) 
    : m_entities()
    , m_nextEntityID(0)
    , m_systems(sys)
    , m_playerID(0)
    , m_category(Category::System)
{}

/**
 * Créer une nouvelle entité vide
 * @return IEntity* pointeur sur la nouvelle entité
 */
IEntity* EntityManager::createEntity()
{
    ++m_nextEntityID;

    IEntity::Ptr newEntity(new IEntity(m_nextEntityID));
    m_entities.emplace(m_nextEntityID, std::move(newEntity));

    return m_entities[m_nextEntityID].get();
}

/**
 * Supprime l'entité dont l'id est passé en paramètre
 * @param id id de l'entité
 */
void EntityManager::removeEntity(unsigned int id)
{
    auto entity = m_entities.find(id);
    assert(entity != m_entities.end());
    m_entities.erase(id);
}

/**
 * Supprime toutes les entitées
 */
void EntityManager::clearAllEntity()
{
    m_entities.clear();
    m_nextEntityID = 0;
}

/**
 * Renvoi l'entité dont l'id correspond au paramètre
 * @param  id id de l"ntité
 * @return    l'entié
 */
IEntity& EntityManager::getEntity(const unsigned int id) const
{
    auto ent = m_entities.find(id);
    assert(ent != m_entities.end());
    return *ent->second;
} 

/**
 * Renvois le nombre d'entité enregistrer 
 * @return nombre d'netité
 */
unsigned int EntityManager::getNbEntiy() const
{
    return static_cast<unsigned int>(m_entities.size());
}

/**
 * Retourne l'id du joueur
 * @return id du joueur
 */
unsigned int EntityManager::getPlayerID() const
{
    return m_playerID;
}

/**
 * Retourne la liste des entitées
 */
std::vector<IEntity*> EntityManager::getEntityList() const
{
    std::vector<IEntity*> entities;
    for (const auto& e : m_entities)
        entities.push_back(e.second.get());
    return entities;
}

IEntity& EntityManager::getPlayer() const
{
    auto ent = m_entities.find(m_playerID);
    assert(ent != m_entities.end());
    return *ent->second;
}

void EntityManager::setEntitySystem(IEntity* entity, unsigned int sys)
{
    m_systems.setupEntity(*entity, sys);
}

void EntityManager::onCommand(const Command& command, sf::Time dt)
{
    for (const auto& e : m_entities)
    {
        if (command.category & e.second->getCategory())
            command.action(e.second.get(), dt);
    }

    if (command.category & m_category)
        command.action(this, dt);
}

void EntityManager::update(sf::Time dt)
{
    std::vector<IEntity*> entities;

    for (const auto& e : m_entities)
        entities.push_back(e.second.get());

    m_systems.update(dt, entities);
}