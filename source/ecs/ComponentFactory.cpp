#include "ComponentFactory.hpp"

#include <cassert>

/**
 * Initialise la componenet factory
 */
void ComponentFactory::init()
{
    this->registerComponent<Movement>(Component::ID::Movement);
    this->registerComponent<Position>(Component::ID::Position);
    this->registerComponent<Direction>(Component::ID::Direction);
    this->registerComponent<Shape>(Component::ID::Shape);
    this->registerComponent<Sprite>(Component::ID::Sprite);
    this->registerComponent<Health>(Component::ID::Health);
    this->registerComponent<Armor>(Component::ID::Armor);
    this->registerComponent<Info>(Component::ID::Info);
    this->registerComponent<Script>(Component::ID::Script);
    this->registerComponent<LifeTime>(Component::ID::LifeTime);
}

/**
 * Retourne un pointeur sur le nouveaux compossant créer 
 * à partir de l'id donnée
 * @param  Component::ID id du composant
 * @return               pointeur sur le nouveau composant
 */
IComponent::Ptr ComponentFactory::getComponent(const Component_ID id)
{
    auto found = m_factory.find(id);
    assert(found != m_factory.end());

    return found->second();
}

/**
 * Retourne le nombre de composant enregistrer
 * @return unsigned int nb de composant
 */
std::size_t ComponentFactory::getNbComponent() const
{
    return m_factory.size();
}