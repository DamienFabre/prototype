#ifndef COMPONENT_LIST_HPP_INCLUDED
#define COMPONENT_LIST_HPP_INCLUDED

#include <string>

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/System/Time.hpp>

#include "IComponent.hpp"
#include "ECSIdentifiers.hpp"

struct Position : IComponent
{
    Position(const Component_ID id) : IComponent(id) {}

    sf::Vector2f position = {0.f, 0.f};
};

struct Movement : IComponent
{
    Movement(const Component_ID id) : IComponent(id) {}

    sf::Vector2f velocity = {0.f, 0.f};
    float        speed    = 1.f;
};

struct Direction : IComponent
{
    Direction(const Component_ID id) : IComponent(id) {}

    sf::Vector2f direction = {0.f, 0.f};
};

struct Shape : IComponent
{
    Shape(const Component_ID id) : IComponent(id) {}

    sf::CircleShape shape;
};

struct Sprite : IComponent
{
    Sprite(const Component_ID id) : IComponent(id) {}

    sf::Sprite sprite;
};

struct Health : IComponent
{
    Health(const Component_ID id) : IComponent(id) {}

    float health = 0.f;
};

struct Armor : IComponent
{
    Armor(const Component_ID id) : IComponent(id) {}

    float armor = 0.f;
};

struct Info : IComponent
{
    Info(const Component_ID id) : IComponent(id) {}

    std::string name = "";
    std::string description = "";
};

struct Script : IComponent
{
    Script(const Component_ID id) : IComponent(id) {}

    std::string scriptName = "";
};

struct LifeTime : IComponent
{
    LifeTime(const Component_ID id) : IComponent(id) {}

    sf::Time lifeTime;
    bool useIt = false;
};

#endif // COMPONENT_LIST_HPP_INCLUDED