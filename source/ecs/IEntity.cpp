#include "IEntity.hpp"

#include <algorithm>

/**
 * Constructeur
 */
IEntity::IEntity(const unsigned int id) 
    : m_entityID(id)
    , m_components()
    , m_category(Category::None)
{}

/**
 * Vérifie cette entité contient bien le composant spécifier
 * @param  typeID id du composant
 * @return        booléen indiquant la présence ou non du composant
 */
bool IEntity::hasComponent(Component_ID id)
{
    auto found = std::find_if(m_components.begin(), m_components.end(),
        [=] (IComponent::Ptr& comp) { return id == comp->getTypeID(); });

    return (found != m_components.end()) ? true : false;
}

/**
 * Retourne l'id de l'entité
 * @return id
 */
unsigned int IEntity::getID() const
{
    return m_entityID;
}

/**
 * Ajoute un composant à l'entité
 * @param id id du composant
 */
void IEntity::addComponent(IComponent::Ptr comp)
{
    auto found = std::find_if(m_components.begin(), m_components.end(),
        [&] (const IComponent::Ptr& c) { return comp->getTypeID() == c->getTypeID(); });
    assert(found == m_components.end());
    
    m_components.push_back(std::move(comp));
}

void IEntity::setCategory(unsigned int category)
{
    m_category = category;
}

unsigned int IEntity::getCategory() const
{
    return m_category;
}
