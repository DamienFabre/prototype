#include "GameLogicSystem.hpp"

#include <type_traits>
#include <cassert>
#include <iostream>

#include "../lua/LuaContext.hpp"
#include "../events/Command.hpp"
#include "../events/CommandQueue.hpp"
#include "EntityManager.hpp"

/**
 * Constructeur
 */
GameLogicSystem::GameLogicSystem(System::ID id) 
    : ISystem(id)
    , m_lua(*LuaContext::getInstance())
{
}

/**
 * Initialise le systeme
 */
void GameLogicSystem::init()
{
}

/**
 * Met à jour tous les systemes enregistrer
 * @param dt       temps écoulé
 * @param entities tableau d'entité à mettre à jour
 */
void GameLogicSystem::update(sf::Time dt, std::vector<IEntity*>& entities)
{
    for (auto& ent : entities)
    {
        if (ent->hasComponent(Component::ID::Script))      
        {
            const std::string scriptName = ent->getComponent<Script>(Component::ID::Script)
                                              ->scriptName;
            m_lua.callFunction(scriptName, "update", ent);
        }
        if (ent->hasComponent(Component::ID::LifeTime))     
        {
            LifeTime* lt = ent->getComponent<LifeTime>(Component::ID::LifeTime);
            if (lt->useIt)
            {
                lt->lifeTime -= dt;
                if (lt->lifeTime < sf::Time::Zero) {
                    Command c;
                    c.category = Category::System;
                    c.action = [=] (void* ptr, sf::Time) {
                        EntityManager* em = static_cast<EntityManager*>(ptr);
                        em->removeEntity(ent->getID());
                    };
                    CommandQueue::push(c);
                }
            }
        }
    }
}

/**
 * Ajoute les composant nécéssaire à l'entité pour pouvoir être utilisée 
 * par ce système
 * @param entity à monter
 */
void GameLogicSystem::setupEntity(IEntity& entity, ComponentFactory& factory)
{
    entity.addComponent(factory.getComponent(Component::ID::Script));
    entity.addComponent(factory.getComponent(Component::ID::LifeTime));
}