#ifndef ENTITIES_IDENTIFIERS_HPP_INCLUDED
#define ENTITIES_IDENTIFIERS_HPP_INCLUDED

#include <map>

namespace Entity
{ 
    enum class ID
    {
        None,
        Player,
        Ennemy
    };
} // namespace Entity

using Component_ID = int;
namespace Component
{
    namespace ID
    {
        constexpr int None      = 0,
                      Position  = 1 << 0,
                      Movement  = 1 << 1,
                      Direction = 1 << 2,
                      Shape     = 1 << 3,
                      Sprite    = 1 << 4,
                      Health    = 1 << 5,
                      Armor     = 1 << 6,
                      Info      = 1 << 7,
                      Script    = 1 << 8,
                      LifeTime  = 1 << 9;
    } // namespace ID
} // namespace Component

namespace System
{
    enum ID
    {
        None      = 0,
        Movement  = 1 << 1,
        Graphics  = 1 << 2,
        Physics   = 1 << 3, 
        GameLogic = 1 << 4, // script
        Input     = 1 << 5,
        UI        = 1 << 6,
        Audio     = 1 << 7,
        SystemCount
    };

    const std::string system_names[] = { 
        "NONE", 
        "MOVEMENT", 
        "GRAPHICS", 
        "PHYSICS", 
        "GAMELOGIC", 
        "INPUT", 
        "UI", 
        "AUDIO", 
        "SYSTEMCOUNT"
    };

    const std::map<unsigned int, std::string> enumMap = {
        { ID::None, "NONE" },
        { ID::Movement, "MOVEMENT" },
        { ID::Graphics, "GRAPHICS" },
        { ID::GameLogic, "GAMELOGIC" }
    };
} // namespace System

#endif // ENTITIES_IDENTIFIERS_HPP_INCLUDED