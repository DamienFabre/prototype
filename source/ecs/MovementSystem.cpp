#include "MovementSystem.hpp"
#include <iostream>
/**
 * Constructeur
 */
MovementSystem::MovementSystem(System::ID id) : ISystem(id)
{
}

/**
 * Initialise le systeme
 */
void MovementSystem::init()
{
}

/**
 * Met à jour tous les systemes enregistrer
 * @param dt       temps écoulé
 * @param entities tableau d'entité à mettre à jour
 */
void MovementSystem::update(sf::Time dt, std::vector<IEntity*>& entities)
{
    for (auto& ent : entities)
    {
        Movement* vel = ent->getComponent<Movement>(Component::ID::Movement);
        Position* pos = ent->getComponent<Position>(Component::ID::Position);

        pos->position += vel->velocity * vel->speed * dt.asSeconds();
        // std::cout << vel->speed << std::endl;
        if (ent->hasComponent(Component::ID::Sprite))
        {
            Sprite* spr = ent->getComponent<Sprite>(Component::ID::Sprite);
            spr->sprite.setPosition(pos->position);
        }
    }
}

/**
 * Ajoute les composant nécéssaire à l'entité pour pouvoir être utilisée 
 * par ce système
 * @param entity à monter
 */
void MovementSystem::setupEntity(IEntity& entity, ComponentFactory& factory)
{
    entity.addComponent(factory.getComponent(Component::ID::Movement));
    entity.addComponent(factory.getComponent(Component::ID::Position));
}
