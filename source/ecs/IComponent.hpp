#ifndef I_COMPONENT_HPP_INCLUDED
#define I_COMPONENT_HPP_INCLUDED

#include <memory>

#include "ECSIdentifiers.hpp"

class IComponent
{
public:

    using Ptr = std::unique_ptr<IComponent>;

    /**
     * Constructeur
     */
    IComponent(const Component_ID id) noexcept : m_typeID(id) {};
    virtual ~IComponent() = default;

    /**
     * Retourne l'id du compsant
     * @return typeId 
     */
    Component_ID getTypeID() const { return m_typeID; };
    // void setTypeID(const Component::ID typeID);

private:

    Component_ID m_typeID;
};

#endif // I_COMPONENT_HPP_INCLUDED