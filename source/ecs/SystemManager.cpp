#include "SystemManager.hpp"

#include <cassert>

#include "IEntity.hpp"
#include "ComponentFactory.hpp"
#include "SystemList.hpp"

/**
 * Constructeur
 */
SystemManager::SystemManager(ComponentFactory& factory)
    : m_systems()
    , m_compFactory(factory)
 {
 }

/**
 * Initialise le systeme manager
 */
void SystemManager::init()
{
    this->addSystem<MovementSystem>(System::ID::Movement);
    this->addSystem<GraphicsSystem>(System::ID::Graphics);
    this->addSystem<GameLogicSystem>(System::ID::GameLogic);
}

/**
 * Supprime le systeme dont l'id est passé en paramètre
 * @param id     [description]
 * @param system [description]
 */
void SystemManager::removeSystem(System::ID id)
{
    auto found = m_systems.find(id);
    assert(found != m_systems.end());
    m_systems.erase(found);
}

/**
 * Update tous les systems
 * @param dt temps écoulé depuis le dernier appel
 */
void SystemManager::update(sf::Time dt, std::vector<IEntity*>& entities)
{
    for (auto& sys : m_systems)
        sys.second->update(dt, entities);
}

/**
 * Ajoute les composants nécessaire à l'entité donné
 * @param id     id du system
 * @param entity entité à construire
 */
void SystemManager::setupEntity(System::ID id, IEntity& entity)
{
    auto found = m_systems.find(id);
    assert(found != m_systems.end());
    found->second->setupEntity(entity, m_compFactory);
}

void SystemManager::setupEntity(IEntity& entity, unsigned int systems)
{
    for (const auto& pair : m_systems)
        if (pair.first & systems)
            pair.second->setupEntity(entity, m_compFactory);
}

/**
 * Retourne un booleen indiquant si le conteneur est vide
 * @return vrai si le conteneur est vide
 */
bool SystemManager::isEmpty()
{
    return m_systems.empty();
}