#ifndef I_ENTITY_HPP_INCLUDED
#define I_ENTITY_HPP_INCLUDED

#include <vector>
#include <memory>
#include <cassert>

#include "ECSIdentifiers.hpp"
#include "IComponent.hpp"
#include "../events/Category.hpp"

class IEntity 
{
public:

    using Ptr = std::unique_ptr<IEntity>;

    IEntity(const unsigned int id);
    virtual ~IEntity() = default;

    void addComponent(IComponent::Ptr comp);
    bool hasComponent(Component_ID id);

    template <typename T>
    T* getComponent(Component_ID id) const;
    
    unsigned int getID() const;

    void setCategory(unsigned int category);
    unsigned int getCategory() const;

protected:

    const unsigned int m_entityID;
    std::vector<IComponent::Ptr> m_components = {};
    unsigned int m_category = Category::None;
};

/**
 * Retourne le composant demander 
 * @param  id id du composant
 * @return    composant
 */
template <typename T>
T* IEntity::getComponent(Component_ID id) const
{
    T* ptr = nullptr;
    for (const auto& comp : m_components)
        if (comp->getTypeID() == id)
        {
            IComponent* p = comp.get();
            assert(dynamic_cast<T*>(p) != nullptr);
            ptr = static_cast<T*>(p);
        }
    return ptr;
}

#endif // I_ENTITY_HPP_INCLUDED