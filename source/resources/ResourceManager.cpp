#include "ResourceManager.hpp"

#include "../lua/LuaContext.hpp"

namespace
{
    enum rType
    {
        Font,
        Texture,
        Sound,
        Music,
        Count
    };

    std::array<std::map<std::string, std::string>, Count> resources;

    int l_load_texture(lua_State *l)
    {
        luaL_checktype(l, 1, LUA_TTABLE);
        const std::string& id = LuaContext::checkStringField(l, 1, "id");
        const std::string& file = LuaContext::checkStringField(l, 1, "file");
        resources[rType::Texture].emplace(id, file);

        return 0;
    }

    int l_load_font(lua_State *l)
    {
        luaL_checktype(l, 1, LUA_TTABLE);
        const std::string& id = LuaContext::checkStringField(l, 1, "id");
        const std::string& file = LuaContext::checkStringField(l, 1, "file");
        resources[rType::Font].emplace(id, file);

        return 0;
    }

    int l_load_sound(lua_State *l)
    {
        luaL_checktype(l, 1, LUA_TTABLE);
        const std::string& id = LuaContext::checkStringField(l, 1, "id");
        const std::string& file = LuaContext::checkStringField(l, 1, "file");  
        resources[rType::Sound].emplace(id, file);

        return 0;   
    }

    int l_load_music(lua_State *l)
    {
        luaL_checktype(l, 1, LUA_TTABLE);
        const std::string& id = LuaContext::checkStringField(l, 1, "id");
        const std::string& file = LuaContext::checkStringField(l, 1, "file");  
        resources[rType::Music].emplace(id, file);

        return 0;   
    }
}

void ResourceManager::init(const std::string& folder, bool usePhysFs)
{
    setFolderName(folder);
    setPhysFs(usePhysFs);
}

void ResourceManager::loadAllResources()
{
    lua_State *l = luaL_newstate();
    lua_register(l, "texture", l_load_texture);
    lua_register(l, "font", l_load_font);
    lua_register(l, "sound", l_load_sound);
    lua_register(l, "music", l_load_music);
    luaL_dofile(l, "assets/Textures/textures.dat");
    luaL_dofile(l, "assets/Fonts/fonts.dat");
    luaL_dofile(l, "assets/Sounds/sounds.dat");
    luaL_dofile(l, "assets/Musics/musics.dat");
    lua_close(l);

    for (const auto& pair : resources[rType::Texture])
        m_textures.load(pair.first, pair.second);

    for (const auto& pair : resources[rType::Font])
        m_fonts.load(pair.first, pair.second);

    for (const auto& pair : resources[rType::Sound])
        m_sounds.getSoudBufferHolder().load(pair.first, pair.second);

    m_musics.init(resources[rType::Music], m_usePhysFs);
}

void ResourceManager::setFolderName(const std::string& folder)
{
    m_textures.setFolderName(folder + "/Textures/");
    m_fonts.setFolderName(folder + "/Fonts/");
    m_sounds.getSoudBufferHolder().setFolderName(folder + "/Sounds/");
}

void ResourceManager::setPhysFs(bool usePhysFs)
{
    m_usePhysFs = usePhysFs;
    m_textures.setPhysFs(usePhysFs);
    m_fonts.setPhysFs(usePhysFs);
    m_sounds.getSoudBufferHolder().setPhysFs(usePhysFs);
}

TextureHolder& ResourceManager::getTextureHolder()
{
    return m_textures;
}

FontHolder& ResourceManager::getFontHolder()
{
    return m_fonts;
}

MusicPlayer& ResourceManager::getMusicPlayer()
{   
    return m_musics;
}

SoundPlayer& ResourceManager::getSoudPlayer()
{
    return m_sounds;
}
