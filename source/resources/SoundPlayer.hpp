#ifndef SOUNDPLAYER_HPP_INCLUDED
#define SOUNDPLAYER_HPP_INCLUDED

#include <list>

#include <SFML/Audio/SoundBuffer.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/System/Vector2.hpp>

#include "ResourceHolder.hpp"
#include "ResourceIdentifiers.hpp"

class SoundPlayer
{
public:

    SoundPlayer() = default;
    ~SoundPlayer() = default;

    void play(const std::string& effect);
    void play(const std::string& effect, sf::Vector2f position);

    void removeStoppedSounds();
    void setListenerPosition(sf::Vector2f position);
    sf::Vector2f getListenerPosition() const;

    SoundBufferHolder& getSoudBufferHolder();

private:

    SoundBufferHolder    m_soundBuffers;
    std::list<sf::Sound> m_sounds;
};

#endif // SOUNDPLAYER_HPP_INCLUDED