
template <typename Resource, typename Identifier>
void ResourceHolder<Resource, Identifier>::load(Identifier id, const std::string& filename)
{
    // on créer un pointeur sur la ressource puis on ouvre un flux vers le
    // fichier demander une erreur est envoyer si le fichier n'a pas put être charger
    std::unique_ptr<Resource> resource(new Resource());
    const std::string file = m_folderName + filename;

    if (m_useFhysFs) 
    {
        const std::string subfile = file.substr(file.find_first_of("/"), file.length());
        m_physFsStream.open(subfile.c_str());
        if (!resource->loadFromStream(m_physFsStream))
            throw std::runtime_error("ResourceHolder::load - Failed to load " + file);
    }
    else 
    {
        if (!resource->loadFromFile(file))
            throw std::runtime_error("ResourceHolder::load - Failed to load " + file);
    }

    // on vérifie que le fichier à bien été ajouter
    auto inserted = m_resourceMap.insert(std::make_pair(id, std::move(resource)));
    assert(inserted.second);
    LOG_INFO("Chargement du fichier : " + file);
}

template <typename Resource, typename Identifier>
template <typename Parameter>
void ResourceHolder<Resource, Identifier>::load(Identifier id, const std::string& filename, const Parameter& secondParam)
{
    // on créer un pointeur sur la ressource puis on ouvre un flux vers le
    // fichier demander une erreur est envoyer si le fichier n'a pas put être charger
    std::unique_ptr<Resource> resource(new Resource());
    const std::string file = m_folderName + filename;

    if (m_useFhysFs) 
    {
        const std::string subfile = file.substr(file.find_first_of("/"), file.length());
        m_physFsStream.open(subfile.c_str());
        if (!resource->loadFromStream(m_physFsStream, secondParam))
            throw std::runtime_error("ResourceHolder::load - Failed to load " + file);
    }
    else 
    {
        if (!resource->loadFromFile(file, secondParam))
            throw std::runtime_error("ResourceHolder::load - Failed to load " + file);
    }

    // if (!resource->loadFromFile(filename, secondParam))
    //    throw std::runtime_error("ResourceHolder::load - Failed to load " + filename);

    // on vérifie que le fichier à bien été ajouter
    auto inserted = m_resourceMap.insert(std::make_pair(id, std::move(resource)));
    assert(inserted.second);
    LOG_INFO("Chargement du fichier : " + file);
}

template <typename Resource, typename Identifier>
Resource& ResourceHolder<Resource, Identifier>::get(Identifier id)
{
    // on cherche le fichier demander
    auto found = m_resourceMap.find(id);
    assert(found != m_resourceMap.end());

    return *found->second;
}

template <typename Resource, typename Identifier>
const Resource& ResourceHolder<Resource, Identifier>::get(Identifier id) const
{
    // on cherche le fichier demander
    auto found = m_resourceMap.find(id);
    assert(found != m_resourceMap.end());

    return *found->second;
}
