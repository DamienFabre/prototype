#include "MusicPlayer.hpp"

#include "PhysFsStream.hpp"

MusicPlayer::~MusicPlayer()
{
    delete m_physFsStream;
}

/**
 * Initialise la classe en eregistrant les musiques
 */
void MusicPlayer::init(const std::map<std::string, std::string>& fileName, bool usePhysFs)
{
    m_fileName = fileName;
    m_usePhysFs = usePhysFs;
    m_physFsStream = new PhysFsStream;
}

/**
 * Joue la music dont l'id est donné
 * @param theme id de la musique
 */
void MusicPlayer::play(const std::string& theme)
{
    std::string file = m_fileName[theme];
    // m_physFsStream = new PhysFsStream;

    if (m_usePhysFs)
    {
        const std::string subfile = file.substr(file.find_first_of("/"), file.length());
        m_physFsStream->open(subfile.c_str());
        if (!m_music.openFromStream(*m_physFsStream))
            throw std::runtime_error("Music " + file + " could not be loaded.");
    }
    else
    {
        if (!m_music.openFromFile(file))
            throw std::runtime_error("Music " + file + " could not be loaded.");        
    }

    m_music.setVolume(m_volume);
    m_music.setLoop(true);
    m_music.play();
}

/**
 * Arrete de jouer la musique en cours
 */
void MusicPlayer::stop()
{
    m_music.stop();
}

/**
 * Met en pause la musique
 * @param paused vrai -> met en pause faux -> reprend la musique
 */
void MusicPlayer::setPaused(bool paused)
{
    if (paused)
        m_music.pause();
    else
        m_music.play();
}

/**
 * Modifie le volume de la musique
 * @param volume nouvelle valeur
 */
void MusicPlayer::setVolume(float volume)
{
    m_volume = volume;
}