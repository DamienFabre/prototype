#ifndef MUSICPLAYER_HPP_INCLUDED
#define MUSICPLAYER_HPP_INCLUDED

#include <map>
#include <string>

#include <SFML/Audio/Music.hpp>

// #include "ResourceHolder.hpp"
// #include "ResourceIdentifiers.hpp"
// #include "PhysFsStream.hpp"

class PhysFsStream;

class MusicPlayer
{
public:

    MusicPlayer() = default;
    ~MusicPlayer();

    void init(const std::map<std::string, std::string>& fileName, bool usePhysFs);

    void play(const std::string& theme);
    void stop();

    void setPaused(bool paused);
    void setVolume(float volume);

private:

    sf::Music                          m_music;
    std::map<std::string, std::string> m_fileName = {};
    float                              m_volume = 100.f;
    bool                               m_usePhysFs = false;
    PhysFsStream*                      m_physFsStream;
};

#endif // MUSICPLAYER_HPP_INCLUDED