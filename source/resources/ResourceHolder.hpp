#ifndef RESOURCEHOLDER_H
#define RESOURCEHOLDER_H

#include <map>
#include <string>
#include <memory>
#include <stdexcept>
#include <cassert>

#include "PhysFsStream.hpp"
#include "../utility/Logger.hpp"

template <typename Resource, typename Identifier>
class ResourceHolder
{
public:

    ResourceHolder() = default;
    ~ResourceHolder() = default;

    inline void setPhysFs(bool usePhysFs) { m_useFhysFs = usePhysFs; }
    inline void setFolderName(const std::string& folder) { m_folderName = folder; }

    /**
     * Charge en mémoire la resource demander
     * @param id       identifiant de la resource
     * @param filename nom du fichier
     */
    void load(Identifier id, const std::string& filename);

    template <typename Parameter>
    void load(Identifier id, const std::string& filename, const Parameter& secondParam);

    /**
     * Retourne la resource demandé
     * @param  id de la resource
     * @return    une référence sur la resource
     */
    Resource& get(Identifier id);
    const Resource& get(Identifier id) const;

private:

    bool                                            m_useFhysFs = false;
    std::string                                     m_folderName;
    PhysFsStream                                    m_physFsStream;
    std::map<Identifier, std::unique_ptr<Resource>> m_resourceMap;
};

#include "ResourceHolder.inl"
#endif //RESOURCEHOLDER_H
