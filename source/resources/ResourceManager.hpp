#ifndef RESOURCE_MANAGER_HPP_INCLUDED
#define RESOURCE_MANAGER_HPP_INCLUDED

#include "../utility/NonCopyable.hpp"
#include "ResourceIdentifiers.hpp"
#include "ResourceHolder.hpp"
#include "MusicPlayer.hpp"
#include "SoundPlayer.hpp"

class ResourceManager : NonCopyable
{
public:

    ResourceManager() = default;
    ~ResourceManager() = default;

    void init(const std::string& folder, bool usePhysFs);
    void loadAllResources();

    // Setter 
    void setFolderName(const std::string& folder);
    void setPhysFs(bool usePhysFs);

    // Getter
    TextureHolder& getTextureHolder();
    FontHolder& getFontHolder();
    MusicPlayer& getMusicPlayer();
    SoundPlayer& getSoudPlayer();

private:

    bool m_usePhysFs = false;

    TextureHolder m_textures;
    FontHolder    m_fonts;
    MusicPlayer   m_musics;
    SoundPlayer   m_sounds;
};

#endif // RESOURCE_MANAGER_HPP_INCLUDED