#include <iostream>
#include <physfs.h>
#include <SFML/Graphics.hpp>
#include <memory>

class PhysFsStream : public sf::InputStream
{
public:

    PhysFsStream(const char* filename=nullptr);
    virtual ~PhysFsStream();

    bool isOpen() const;
    bool open(const char* filename);
    void close();

    virtual sf::Int64 read(void* data, sf::Int64 size);
    virtual sf::Int64 seek(sf::Int64 position);
    virtual sf::Int64 tell();
    virtual sf::Int64 getSize();

private:

    PHYSFS_File*    m_file;
};