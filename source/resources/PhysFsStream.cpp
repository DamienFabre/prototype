#include "PhysFsStream.hpp"

PhysFsStream::PhysFsStream(const char* filename) 
    : m_file(nullptr)
{
    if (filename)
        open(filename);
}

PhysFsStream::~PhysFsStream()
{
    close();
}

bool PhysFsStream::isOpen() const
{
    return (m_file != nullptr);
}

bool PhysFsStream::open(const char* filename)
{
    if (not isOpen())
        PHYSFS_close(m_file);
    m_file = PHYSFS_openRead(filename);
    if (m_file == NULL)
        std::cout << PHYSFS_getLastError() << std::endl;
    return isOpen();
}

void PhysFsStream::close()
{
    if (not isOpen())
        return;

    if (PHYSFS_close(m_file) == 0 )
        std::cout << PHYSFS_getLastError() << std::endl;      
}

sf::Int64 PhysFsStream::read(void* data, sf::Int64 size)
{
    if (not isOpen())
        return -1;

    // PHYSFS_read returns the number of 'objects' read or -1 on error.
    // We assume our objects are single bytes and request to read size
    // amount of them.
    sf::Int64 readed = PHYSFS_read(m_file, data, 1, static_cast<PHYSFS_uint32>(size));
    if (readed == -1){
        std::cout << PHYSFS_getLastError() << std::endl;
        return 0;
    }
    return readed;
}

sf::Int64 PhysFsStream::seek(sf::Int64 position)
{
    if (not isOpen())
        return -1;

    // PHYSFS_seek return 0 on error and non zero on success
    if ( PHYSFS_seek(m_file, position) == 0 ){
        std::cout << PHYSFS_getLastError() << std::endl;
        return -1;
    }
    return position;
}

sf::Int64 PhysFsStream::tell()
{
    if (not isOpen())
        return -1;

    // PHYSFS_tell returns the offset in bytes or -1 on error just like SFML wants.
    sf::Int64 position = PHYSFS_tell(m_file);
    if (position == -1){
        std::cout << PHYSFS_getLastError() << std::endl;
    }
    return position;
}

sf::Int64 PhysFsStream::getSize()
{
    if (not isOpen())
        return -1;

    // PHYSFS_fileLength also the returns length of file or -1 on error just like SFML wants.
    sf::Int64 size = PHYSFS_fileLength(m_file);
    if (size == -1){
        std::cout << PHYSFS_getLastError() << std::endl;
    }
    return size;
}