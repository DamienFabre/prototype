#ifndef INCLUDE_RESSOURCEIDENTIFIERS_HPP
#define INCLUDE_RESSOURCEIDENTIFIERS_HPP

#include <string>

// Forward déclartions of sfml texture
namespace sf
{
    class Texture;
    class Font;
    class SoundBuffer;
} // namespace sf

namespace Textures
{
    enum class ID
    {
        Entities,
        Jungle,
        TitleScreen,
        Buttons,
        Explosion,
        Particle,
        FinishLine,
        Background1,
        Bullet2,
        Enemy1,
        Raptor
    };
} // namespace Textures

namespace Fonts
{
    enum class ID
    {
        Main,
        Stats
    };
} // namespace Fonts

namespace Music
{
    enum class ID
    {
        MenuTheme,
        MissionTheme
    };
} // namespace Music

namespace SoundEffect
{
    enum class ID
    {
        AlliedGunfire,
        EnemyGunfire,
        Explosion1,
        Explosion2,
        LaunchMissile,
        CollectPickup,
        Button
    };
} // namespace SoundEffect

// namespace Textures_ID
// {
//     constexpr int TitleScreen = 0,
//                   Buttons     = 1,
//                   Raptor      = 2;
// }

// Forward declaration
template <typename Resource, typename Identifier>
class ResourceHolder;


using TextureHolder     = ResourceHolder<sf::Texture, std::string>;
using FontHolder        = ResourceHolder<sf::Font, std::string>;
using SoundBufferHolder = ResourceHolder<sf::SoundBuffer, std::string>;

// typedef ResourceHolder<sf::Texture, std::string>         TextureHolder;
// typedef ResourceHolder<sf::Font, std::string>            FontHolder;
// typedef ResourceHolder<sf::SoundBuffer, SoundEffect::ID> SoundBufferHolder;

#endif //INCLUDE_RESSOURCEIDENTIFIERS_HPP