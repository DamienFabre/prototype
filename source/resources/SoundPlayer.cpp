#include "SoundPlayer.hpp"

#include <cmath>

#include <SFML/Audio/Listener.hpp>

namespace
{
    const float ListenerZ = 150.f;
    const float Attenuation = 8.f;
    const float MinDistance2D = 200.f;
    const float MinDistance3D = std::sqrt(MinDistance2D*MinDistance2D + ListenerZ*ListenerZ);
}

// SoundPlayer::~SoundPlayer()
// {
//     m_sounds.clear();
// }

/**
 * Joue l'effet dont l'id est donné
 * @param effect id de l'effet
 */
void SoundPlayer::play(const std::string& effect)
{
    play(effect, getListenerPosition());
}

/**
 * Joue l'effet dont l'id est donné
 * @param effect   id de l'effet
 * @param position position ou jouer le son
 */
void SoundPlayer::play(const std::string& effect, sf::Vector2f position)
{
    m_sounds.push_back(sf::Sound());
    sf::Sound& sound = m_sounds.back();

    sound.setBuffer(m_soundBuffers.get(effect));
    sound.setPosition(position.x, -position.y, 0.f);
    sound.setAttenuation(Attenuation);
    sound.setMinDistance(MinDistance3D);

    sound.play();
}

/**
 * Supprime de la liste tous les sons terminés
 */
void SoundPlayer::removeStoppedSounds()
{
    m_sounds.remove_if([] (const sf::Sound& s)
    {
        return s.getStatus() == sf::Sound::Stopped;
    });
}

/**
 * Definie la position du listener
 * @param position position du listener
 */
void SoundPlayer::setListenerPosition(sf::Vector2f position)
{
    sf::Listener::setPosition(position.x, -position.y, ListenerZ);
}

/**
 * Retourne la position actuel du listener
 * @return position du listener
 */
sf::Vector2f SoundPlayer::getListenerPosition() const
{
    sf::Vector3f position = sf::Listener::getPosition();
    return sf::Vector2f(position.x, -position.y);
}

/**
 * Retourne le soundBuffer
 * @return soundBuffer
 */
SoundBufferHolder& SoundPlayer::getSoudBufferHolder()
{
    return m_soundBuffers;
}