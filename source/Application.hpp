#ifndef APPLICATION_HPP_INCLUDED
#define APPLICATION_HPP_INCLUDED

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "lua/LuaContext.hpp"
#include "utility/Logger.hpp"
#include "utility/NonCopyable.hpp"
#include "states/StateManager.hpp"
#include "resources/ResourceManager.hpp"
#include "Player.hpp"

#include "particle/ParticleSystem.hpp"

namespace sf
{
    class Time;
}

class Application : NonCopyable
{
public:

    explicit Application();
    ~Application() = default;

    void init();
    void run();

    void takeScreenshot() const;
    sf::Vector2f getMousePosition() const;

private:

    void initWindow();
    void registerStates();
    
    void processEvents();
    void update(sf::Time dt);
    void render();

    static const sf::Time TimePerFrame;

    sf::RenderWindow      m_window;
    ResourceManager       m_resourceManager;
    Player                m_player;
    LuaContext            m_luaContext;
    StateManager          m_stateManager;

    bool                  m_isFocus   = true;
    bool                  m_isInit    = false;
    float                 m_totalTime = 0.f;    
    std::string           m_windowTitle;
};

#endif // APPLICATION_HPP_INCLUDED

