-- level test

-- generat_level()

-- map = {
--   tile = {
--     sprite_id = "floor",
--     coord = { x = 10, y = 5 },
--     size = { x = 3, y = 3 }
--   },
--   tile = {
--     sprite_id = "floor",
--     coord = { x = 10, y = 5 },
--     size = { x = 3, y = 3 }
--   }
-- }

-- map.add_sprite("floor", 50, 0)
-- map.add_sprite("wall", 156, 452)
-- api.map.reset()

-------------------------------------------------------------------------------------
---[[
-- print("début du script")

dofile("scripts/data_structure.lua")
local util = dofile("scripts/util.lua")

math.randomseed(os.time())

local MAP_WIDTH, MAP_HEIGHT = 26, 19
local roughness = 50 -- How much the cave varies in width from 1 to 100.
local windyness = 50 -- How much the cave varies in positioning from 1 to 100.

local starting_width = math.random(3, MAP_WIDTH-10)
local starting_x, starting_y = MAP_WIDTH/2 - starting_width/2, 0 -- point de départ 
local x, y, width = starting_x, starting_y, starting_width

local VIDE, MUR, PIECE = "vide", "wall", "floor"
local level = matrice.new(MAP_HEIGHT, MAP_WIDTH, MUR)

local function print_map(t)
  local str = ""
  for i = 1, MAP_HEIGHT do
    for j = 1, MAP_WIDTH do
      str = str .. t[i][j]
    end
    print(str)
    str = ""
  end
end

local function draw_map(level)
  for i = 1, MAP_HEIGHT do
    for j = 1, MAP_WIDTH do
      map.add_sprite(level[i][j], (j - 1) * 32, (i - 1) * 32)
    end
  end
end

local function generate_vertical()
  local length = MAP_HEIGHT
  for i = 1, length do
    y = y + 1
    if math.random(0, 100) <= roughness then
      local r = nil
      repeat 
        r = math.random(-2, 2)
      until r ~= 0
      width = width + r
      if width < 3 then width = 3 end
      if width > MAP_WIDTH then width = MAP_WIDTH end
    end

    if math.random(0, 100) <= windyness then
      local r = nil
      repeat 
        r = math.random(-2, 2)
      until r ~= 0
      x = x + r
      if x < 0 then x = 0 end
      if x > MAP_WIDTH - 3 then x = MAP_WIDTH - 3 end
    end

    for j = 1, MAP_WIDTH do
      if j >= x and j <= x + width then 
        level[i][j] = PIECE
      end
    end
  end
end

local function generate_horizontal()
  local length = MAP_WIDTH
  for i = 1, length do
    y = y + 1
    if math.random(0, 100) <= roughness then
      local r = nil
      repeat 
        r = math.random(-2, 2)
      until r ~= 0
      width = width + r
      if width < 3 then width = 3 end
      if width > MAP_WIDTH then width = MAP_WIDTH end
    end

    if math.random(0, 100) <= windyness then
      local r = nil
      repeat 
        r = math.random(-2, 2)
      until r ~= 0
      x = x + r
      if x < 0 then x = 0 end
      if x > MAP_WIDTH - 3 then x = MAP_WIDTH - 3 end
    end

    for j = 1, MAP_HEIGHT do
      if j >= x and j <= x + width then 
        level[j][i] = PIECE
      end
    end
  end
end

-- generate_vertical()
generate_horizontal()
-- print_map(map)
draw_map(level)

-- chargement des entitées
scripted_entity.entity_test_1.init(10, 10)
scripted_entity.entity_test_2.init()

-- util.wait(1)
scripted_entity.entity_test_1.init(50, 50)


-- print("fin du script")
--]]