-- Fonctions utilitaire

util = {}

-- retourne la longueur d'un vecteur
function util.length(vec)
  return ((vec.x^2 + vec.y^2)^0.5)
end

-- retourne un vecteur unitaire
function util.unit_vector(vec)
  local len = util.length(vec)
  local vx = vec.x / len
  local vy = vec.y / len
  return {x = vx, y = vy}
end

function util.test()
  print("util test")
end

function util.wait(n)
  local t0 = os.clock()
  while os.clock() - t0 <= n do end
end
