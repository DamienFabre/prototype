objects = {}

-- Munition
objects.bullet = {
  system = bit32.bor(MOVEMENT, GRAPHICS, GAMELOGIC),
  speed = 500,
  sprite = "bullet",
  script = "empty_entity",
}

objects.laser = {
  system = bit32.bor(MOVEMENT, GRAPHICS, GAMELOGIC),
  speed = 30,
  sprite = "laser",
  script = "empty_entity",
}