-- Initailise l'environnement lua
if not v then v = {} end

dofile("scripts/data_structure.lua")
dofile("scripts/constantes.lua")

local lfs = require("lfs")

function load_all_script_with_own_env(path)
  for file in lfs.dir(path) do
    if file ~= "." and file ~= ".." then
      local filepath = path..'/'..file
      local attr = lfs.attributes(filepath)
      assert (type(attr) == "table")
      if attr.mode == "file" then
        file = string.match(file, "[^.]+")
        env = {}
        setmetatable(env, {__index = _G})
        scripted_entity[file] = loadfile(filepath, "bt", env)()
      elseif attr.mode == "directory" then
        load_all_script_with_own_env(filepath)
      end
    end
  end
end

scripted_entity = {}
load_all_script_with_own_env("scripts/entities")

function setup_entity(e, stat)
  local speed = stat.speed or 0

  entity.set_system(e, stat.system)
  entity.set_speed(e, speed)
  entity.set_sprite(e, stat.sprite)
  if stat.script then entity.set_script(e, stat.script) end
end