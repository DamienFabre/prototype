-- Une liste de data structure en lua

-------------------------------------------------------------------------------------
-- array 
-------------------------------------------------------------------------------------

local mt_array = {}
mt_array.__index = mt_array

mt_array.__newindex = function(table, key, value)
  error("L'index " .. key .. " n'existe pas dans ce tableau\n")
end

mt_array.__tostring = function(self)
  local str = ""
  for _, v in ipairs(self) do str = str .. v .. ", " end
  return string.sub(str, 1, string.len(str) - 2)
end

mt_array.__eq = function(a, b)
  if #a ~= #b then return false end
  for i, v in ipairs(a) do
    if a[i] ~= b[i] then return false end
  end
  return true
end

function array( length )
  local arr = {}
  for i = 1, length do arr[i] = nil end
  return setmetatable(arr, mt_array)
end

-------------------------------------------------------------------------------------
-- matrice
-------------------------------------------------------------------------------------

matrice = {}

function matrice.new( rows, columns, value )
  local mt = {}
  local v = value or 0
  for i = 1, rows do
    mt[i] = {}
    for j = 1, columns do
      mt[i][j] = v
    end
  end
  return mt
end


-------------------------------------------------------------------------------------
-- queue and double queue
-------------------------------------------------------------------------------------

List = {}

function List.new() 
  return {first = 0, last = -1}
end

function List.pushfirst(list, value)
  local first = list.first - 1
  list.first = first
  list[first] = value
end

function List.pushlast( list, value )
  local last = list.last + 1
  list.last = last
  list[last] = value
end

function List.popfirst( list )
  local first = list.first
  if first > list.last then error("list is empty") end
  local value = list[first]
  list[first] = nil -- to allow garbage collection
  list.first = first + 1
  return value
end

function List.poplast( list )
  local last = list.last
  if list.first > last then error("list is empty") end
  local value = list[last]
  list[last] = nil -- to allow garbage collection
  list.last = last + 1
  return value
end

-- Arbre binaire
-- bt = {}

-- function bt.new()
--   return {parent = nil, left = nil, right = nil}
-- end

-- function bt.add_parent(noeud, p)
--   if noeud.parent ~= nil then 
--     print("ATTENTION le parent actuel va être supprimer !") 
--   end
--   noeud.parent = p
-- end

-- function bt.child_left(noeud, c)
--   noeud.left = c 
  
-- end

-------------------------------------------------------------------------------------
-- Vecteur
-------------------------------------------------------------------------------------

local mtvec = {}

function vec2( a, b )
  return setmetatable({x = a, y = b}, mtvec)
end

function mtvec.__index( self, k )
  if k == "len" then
    return math.sqrt(self.x^2 + self.y^2)
  elseif k == "norm" then
    return self / self.len
  else
    return mtvec[k]
  end
end

function mtvec.__newindex(table, key, value)
  error("L'index " .. key .. " n'existe pas dans ce vecteur\n")
end

-- définition de l'addition de vecteur
function mtvec.__add( a, b )
  return vec2(a.x + b.x, a.y + b.y)
end

-- définition de la soustraction de vecteur
function mtvec.__sub( a, b )
  return vec2(a.x - b.x, a.y - b.y)
end

-- définition de l'aaffichage de vecteur avec la fonction print()
function mtvec.__tostring( self )
  return string.format("(%f, %f)", self.x, self.y)
end

-- définition de la multiplication de vecteur
function mtvec.__mul( a, b )
  if tonumber(a) then
    return vec2(a * b.x, a * b.y)
  elseif tonumber(b) then
    return vec2(b * a.x, b * a.y)
  else
    return vec2(a.x * b.x, a.y * b.y)
  end
end

-- définition de la division de vecteur
function mtvec.__div( a, b )
  if tonumber(a) then
     return vec2(a / b.x, a / b.y)
  elseif tonumber(b) then
     return vec2(a.x / b, a.y / b)
  else
     return vec2(a.x / b.x, a.y / b.y)
  end
end

-- définition de la comparaison '=='
function mtvec.__eq( a, b )
  return (a.x == b.x and a.y == b.y)
end

-------------------------------------------------------------------------------------
-- Stack
-------------------------------------------------------------------------------------

-- GLOBAL
stack = {}

-- Create a Table with stack functions
function stack:create()

  -- stack table
  local t = {}
  -- entry table
  t._et = {}

  -- push a value on to the stack
  function t:push(...)
    if ... then
      local targs = {...}
      -- add values
      for _,v in pairs(targs) do
        table.insert(self._et, v)
      end
    end
  end

  -- pop a value from the stack
  function t:pop(num)

    -- get num values from stack
    local num = num or 1

    -- return table
    local entries = {}

    -- get values into entries
    for i = 1, num do
      -- get last entry
      if #self._et ~= 0 then
        table.insert(entries, self._et[#self._et])
        -- remove last value
        table.remove(self._et)
      else
        break
      end
    end
    -- return unpacked entries
    return unpack(entries)
  end

  -- get entries
  function t:getn()
    return #self._et
  end

  -- list values
  function t:list()
    for i,v in pairs(self._et) do
      print(i, v)
    end
  end
  return t
end