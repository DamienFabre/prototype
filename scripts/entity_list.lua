-- entity test 

local entity_list = {}

entity_list.monster1 = {
  health = 10,
  armor = 5,
  name = "Grrrrh !",
  sprite_id = "wall",
  system = bit32.bor(MOVEMENT, GRAPHICS)
}

entity_list.monster2 = {
  health = 10,
  armor = 5,
  name = "Grrrrh !",
  sprite_id = "floor",
  system = bit32.bor(MOVEMENT, GRAPHICS)
}

return entity_list