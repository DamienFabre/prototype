-- Tous les évènements relatif au joueur

dofile("scripts/util.lua")
dofile("scripts/objects.lua")

function on_shoot(ent)
  local base_x, base_y
  if getmetatable(ent) then
    base_x, base_y = avatar:get_position()
  else
    base_x, base_y = entity.get_position(ent)
  end

  local e = entity.create()
  setup_entity(e, objects.bullet)

  local mx, my = app.get_mouse_position()
  local vec = vec2(mx, my) - vec2(base_x, base_y)
  vec = util.unit_vector(vec)
  entity.set_position(e, base_x, base_y)
  entity.set_velocity(e, vec.x, vec.y)
  entity.set_lifetime(e, 2.5)
end