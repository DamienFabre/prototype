-- entity 2

local entity_2 = {}
local dt = 0

function entity_2.init()
  local ent = entity.create()
  entity.set_system(ent, bit32.bor(MOVEMENT, GRAPHICS, GAMELOGIC))
  entity.set_script(ent, "entity_test_2")
  entity.set_sprite(ent, "vide")
  entity.set_position(ent, 100, 200)
  entity.set_velocity(ent, 0, 10)
  entity.set_speed(ent, 5)
end

entity_2.update = function(self)
  local x, y = entity.get_position(self)
  if y > 350 then 
    entity.set_velocity(self, 0, 0)
    dt = dt + FRAME_TIME
    if dt > 1 then entity.set_velocity(self, 0, -10); dt = 0 end
  elseif y < 50 then 
    entity.set_velocity(self, 0, 0) 
    dt = dt + FRAME_TIME
    if dt > 1 then entity.set_velocity(self, 0, 10); dt = 0 end
  end
end

return entity_2