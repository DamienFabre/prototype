-- entity 1

if not v then v = {} end

local entity_1 = {}
-- local dt = 0

function entity_1.init(x, y)
  local ent = entity.create()
  entity.set_system(ent, bit32.bor(MOVEMENT, GRAPHICS, GAMELOGIC))
  entity.set_script(ent, "entity_test_1")
  entity.set_sprite(ent, "vide")
  entity.set_position(ent, x, y)
  entity.set_velocity(ent, 10, 0)
  entity.set_speed(ent, 8)
end

function entity_1.update(self)
  local x, y = entity.get_position(self)
  if x > 700 then 
    entity.set_velocity(self, 0, 0) 
  end
  --   dt = dt + FRAME_TIME
  --   if dt > 1 then entity.set_velocity(self, -10, 0); dt = 0 end
  -- elseif x < 100 then
  --   entity.set_velocity(self, 0, 0) 
  --   dt = dt + FRAME_TIME
  --   if dt > 1 then entity.set_velocity(self, 10, 0); dt = 0 end
  -- end
end

return entity_1