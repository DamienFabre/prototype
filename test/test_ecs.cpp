#include <boost/test/unit_test.hpp>

#include "../source/ecs/IComponent.hpp"
#include "../source/ecs/ComponentFactory.hpp"
#include "../source/ecs/ComponentList.hpp"
#include "../source/ecs/IEntity.hpp"
#include "../source/ecs/EntityManager.hpp"
#include "../source/ecs/ISystem.hpp"
#include "../source/ecs/SystemManager.hpp"
#include "../source/ecs/MovementSystem.hpp"

struct Fixture
{
    ComponentFactory *cm;
    SystemManager    *sm;
    EntityManager    *em;

    Fixture() : cm(new ComponentFactory)
              , sm(new SystemManager(*cm))
              , em(new EntityManager(*sm)) {}
    Fixture(const Fixture& f) = delete;

    ~Fixture() 
    {
        delete cm;
        delete sm;
        delete em;
    }
};

BOOST_FIXTURE_TEST_SUITE(test_entity_component_system, Fixture);

  BOOST_AUTO_TEST_SUITE(testing_component_factory);
    BOOST_AUTO_TEST_CASE(testing_instanciation)
    {
        BOOST_REQUIRE(cm); // on vérifie que la mémoire est bien alloué

        BOOST_REQUIRE(sm);

        BOOST_REQUIRE(em);
    }

    BOOST_AUTO_TEST_CASE(testing_register_component)
    {
        cm->registerComponent<Movement>(Component::ID::Movement);
        cm->registerComponent<Position>(Component::ID::Position);
        cm->registerComponent<Movement>(Component::ID::Movement);
        BOOST_REQUIRE_EQUAL(cm->getNbComponent(), 2);
    }

    BOOST_AUTO_TEST_CASE(testing_get_component)
    {
        cm->registerComponent<Movement>(Component::ID::Movement);
        cm->registerComponent<Position>(Component::ID::Position);

        IComponent::Ptr ptr1 = cm->getComponent(Component::ID::Movement);
        BOOST_CHECK(ptr1);
        BOOST_REQUIRE_EQUAL(ptr1->getTypeID(), Component::ID::Movement);

        IComponent::Ptr ptr2 = cm->getComponent(Component::ID::Position);
        BOOST_CHECK(ptr1);
        BOOST_REQUIRE_EQUAL(ptr2->getTypeID(), Component::ID::Position);
    }
  BOOST_AUTO_TEST_SUITE_END();
  // BOOST_AUTO_TEST_CASE(testing_id_component)
  // {
  //     BOOST_CHECK_EQUAL(Component::ID::Movement, (1 << 1));
  //     BOOST_CHECK_EQUAL(Component::ID::Position, (1 << 0));
  //     BOOST_CHECK_EQUAL(Component::ID::None, 0);
  // }

  // BOOST_AUTO_TEST_CASE(testing_adding_system)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     BOOST_CHECK(sm->isEmpty());

  //     sm->addSystem<MovementSystem>(System::ID::Movement);
  //     BOOST_CHECK(not sm->isEmpty());
  // }

  // BOOST_AUTO_TEST_CASE(testing_removing_system)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);

  //     sm->addSystem<MovementSystem>(System::ID::Movement);
  //     BOOST_CHECK(not sm->isEmpty());

  //     sm->removeSystem(System::ID::Movement);
  //     BOOST_CHECK(sm->isEmpty());
  // }

  // BOOST_AUTO_TEST_CASE(testing_create_entity)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);

  //     cm->registerComponent<Movement>(Component::ID::Movement);
  //     cm->registerComponent<Position>(Component::ID::Position);

  //     sm->addSystem<MovementSystem>(System::ID::Movement);

  //     em->createEntity({System::ID::Movement});
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 1);

  //     em->createEntity({System::ID::Movement});
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 2);

  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 7);
  // }

  // BOOST_AUTO_TEST_CASE(testing_remove_entity)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);

  //     cm->registerComponent<Movement>(Component::ID::Movement);
  //     cm->registerComponent<Position>(Component::ID::Position);

  //     sm->addSystem<MovementSystem>(System::ID::Movement);

  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 7);

  //     em->removeEntity(3);
  //     em->removeEntity(6);
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 5);

  //     em->removeEntity(1);
  //     em->removeEntity(0);
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 3);

  //     em->createEntity({System::ID::Movement});
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 4);
  // }

  // BOOST_AUTO_TEST_CASE(testing_remove_all_entity)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);

  //     cm->registerComponent<Movement>(Component::ID::Movement);
  //     cm->registerComponent<Position>(Component::ID::Position);

  //     sm->addSystem<MovementSystem>(System::ID::Movement);

  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 7);

  //     em->clearAllEntity();
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 0);
  // }

  // BOOST_AUTO_TEST_CASE(testing_get_entity)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);

  //     cm->registerComponent<Movement>(Component::ID::Movement);
  //     cm->registerComponent<Position>(Component::ID::Position);

  //     sm->addSystem<MovementSystem>(System::ID::Movement);

  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 3);

  //     IEntity& e = em->getEntity(2);
  //     BOOST_REQUIRE_EQUAL(sizeof(IEntity), sizeof(e));

  //     IEntity& f = em->getEntity(0);
  //     BOOST_REQUIRE_EQUAL(sizeof(IEntity), sizeof(f));

  //     IEntity& g = em->getEntity(1);
  //     BOOST_REQUIRE_EQUAL(sizeof(IEntity), sizeof(g));
  // }

  // BOOST_AUTO_TEST_CASE(testing_is_player)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);

  //     cm->registerComponent<Movement>(Component::ID::Movement);
  //     cm->registerComponent<Position>(Component::ID::Position);

  //     sm->addSystem<MovementSystem>(System::ID::Movement);

  //     em->createEntity({System::ID::Movement}, true);
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement}, false);
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 3);

  //     BOOST_REQUIRE_EQUAL(em->getPlayerID(), 0);
  // }

  // BOOST_AUTO_TEST_CASE(testing_has_component)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);

  //     cm->registerComponent<Movement>(Component::ID::Movement);
  //     cm->registerComponent<Position>(Component::ID::Position);

  //     sm->addSystem<MovementSystem>(System::ID::Movement);

  //     em->createEntity({System::ID::Movement});

  //     IEntity& f = em->getEntity(0);
  //     BOOST_REQUIRE_EQUAL(sizeof(IEntity), sizeof(f));
  //     BOOST_REQUIRE(f.hasComponent(Component::ID::Position));
  //     BOOST_REQUIRE(f.hasComponent(Component::ID::Movement));
  //     BOOST_REQUIRE(not f.hasComponent(Component::ID::Direction)); 
  // }

  // BOOST_AUTO_TEST_CASE(testing_initialisation)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);

  //     cm->init();
  //     sm->init();

  //     BOOST_REQUIRE_EQUAL(cm->getNbComponent(), 2);
  //     BOOST_CHECK(not sm->isEmpty());
  // }

  // BOOST_AUTO_TEST_CASE(testing_get_entity_list)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);
  //     cm->init();
  //     sm->init();

  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});
  //     em->createEntity({System::ID::Movement});

  //     std::vector<IEntity*> v = em->getEntityList();
  //     BOOST_REQUIRE_EQUAL(v.size(), 7);

  //     em->removeEntity(3);
  //     em->removeEntity(6);
  //     BOOST_REQUIRE_EQUAL(em->getNbEntiy(), 5);

  //     std::vector<IEntity*> v2 = em->getEntityList();
  //     BOOST_REQUIRE_EQUAL(v2.size(), 5);
  // }

  // BOOST_AUTO_TEST_CASE(testing_entity_get_component)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);
  //     cm->init();
  //     sm->init();

  //     em->createEntity({System::ID::Movement}, true);
  //     IEntity& e = em->getPlayer();
  //     Position* c = e.getComponent<Position>(Component::ID::Position);
  //     BOOST_REQUIRE(c != nullptr);
  //     BOOST_REQUIRE_EQUAL(sizeof(*c), sizeof(Position));
  //     Movement* c1 = e.getComponent<Movement>(Component::ID::Movement);
  //     BOOST_REQUIRE(c1 != nullptr);
  //     BOOST_REQUIRE_EQUAL(sizeof(*c1), sizeof(Movement));

  //     sf::Vector2f v1 = c->position;
  //     sf::Vector2f v2(0.f, 0.f);
  //     BOOST_REQUIRE_EQUAL(v1.x, v2.x);
  //     BOOST_REQUIRE_EQUAL(v1.y, v2.y);
  // }

  // BOOST_AUTO_TEST_CASE(testing_movement_system_update)
  // {
  //     ComponentFactory *cm = new ComponentFactory;
  //     SystemManager *sm = new SystemManager(*cm);
  //     EntityManager *em = new EntityManager(*sm);
  //     cm->init();
  //     sm->init();

  //     em->createEntity({System::ID::Movement}, true);
  //     sf::Time dt = sf::seconds(1.f);
  //     std::vector<IEntity*> el = em->getEntityList();
  //     sm->update(dt, el);

  //     IEntity& e = em->getPlayer();
  //     Position* c = e.getComponent<Position>(Component::ID::Position);
  //     BOOST_REQUIRE(c != nullptr);
  //     BOOST_REQUIRE_EQUAL(sizeof(*c), sizeof(Position));

  //     sf::Vector2f v = {1.f, 1.f};
  //     BOOST_REQUIRE_EQUAL(v.x, c->position.x);
  //     BOOST_REQUIRE_EQUAL(v.y, c->position.y);

  //     sm->update(dt, el);
  //     v = {2.f, 2.f};
  //     BOOST_REQUIRE_EQUAL(v.x, c->position.x);
  //     BOOST_REQUIRE_EQUAL(v.y, c->position.y);
  // }
BOOST_AUTO_TEST_SUITE_END()