/*
 * Ce fichier contient le main des tests de Boost. Il n'y pas besoin de la 
 * modifier hormis le titre du module de test
 * 
 */

#define BOOST_TEST_MODULE "Unit Tests pour Prototype"
#include <boost/test/included/unit_test.hpp>