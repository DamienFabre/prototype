#include <boost/test/unit_test.hpp>

#include "../source/utility/Utility.hpp"

BOOST_AUTO_TEST_SUITE(test_system);
BOOST_AUTO_TEST_CASE(testing_degree_to_radian)
{
    float deg = 150.f;
    float rad = toRad(deg);
    BOOST_REQUIRE(equals(rad, 2.617993878f));
}

BOOST_AUTO_TEST_CASE(testing_radian_to_degree)
{
    float rad = 4.57f;
    float deg = toDeg(rad);
    BOOST_REQUIRE(equals(deg, 261.8417124f));
}

BOOST_AUTO_TEST_CASE(testing_get_max_value)
{
    float max = getMaxValue<float>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<float>::max(), max);

    double max1 = getMaxValue<double>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<double>::max(), max1);

    unsigned long max3 = getMaxValue<unsigned long>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<unsigned long>::max(), max3);

    int max2 = getMaxValue<int>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<int>::max(), max2);
}

BOOST_AUTO_TEST_CASE(testing_get_min_value)
{
    float min = getMinValue<float>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<float>::lowest(), min);

    double max1 = getMinValue<double>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<double>::lowest(), max1);

    unsigned long max3 = getMinValue<unsigned long>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<unsigned long>::lowest(), max3);

    int max2 = getMinValue<int>();
    BOOST_REQUIRE_EQUAL(std::numeric_limits<int>::lowest(), max2);
}

BOOST_AUTO_TEST_CASE(testing_length_vector)
{
    sf::Vector2f v = {5.f, 3.f};
    BOOST_REQUIRE(equals(length(v), 5.830951895f));

    v = {17.f , -5.f};
    BOOST_REQUIRE(equals(length(v), 17.72004515f));
}

BOOST_AUTO_TEST_CASE(testing_unit_vector)
{
    sf::Vector2f v = {5.f, 3.f};
    sf::Vector2f uv = unitVector(v);
    BOOST_REQUIRE(equals(uv.x, 0.8574929257f));
    BOOST_REQUIRE(equals(uv.y, 0.5144957554f));
}
BOOST_AUTO_TEST_SUITE_END();