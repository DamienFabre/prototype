
file(
    GLOB
    test_SRCS 
    *.cpp *.cxx *.cc *.C *.c *.h *.hpp
    ${MAINFOLDER}/source/utility/*.cpp
    ${MAINFOLDER}/source/utility/*.hpp
    ${MAINFOLDER}/source/ecs/*.cpp
    ${MAINFOLDER}/source/ecs/*.hpp
    ${MAINFOLDER}/source/lua/*.cpp
    ${MAINFOLDER}/source/lua/*.hpp
    ${MAINFOLDER}/source/events/*.cpp
    ${MAINFOLDER}/source/events/*.hpp
    ${MAINFOLDER}/source/gui/*.cpp
    ${MAINFOLDER}/source/gui/*.hpp
    ${MAINFOLDER}/source/map/*.cpp
    ${MAINFOLDER}/source/map/*.hpp
    ${MAINFOLDER}/source/particle/*.cpp
    ${MAINFOLDER}/source/particle/*.hpp
    ${MAINFOLDER}/source/resources/*.cpp
    ${MAINFOLDER}/source/resources/*.hpp
    ${MAINFOLDER}/source/states/*.cpp
    ${MAINFOLDER}/source/states/*.hpp
    ${MAINFOLDER}/source/Application.hpp
    ${MAINFOLDER}/source/Application.cpp
    ${MAINFOLDER}/source/Avatar.hpp
    ${MAINFOLDER}/source/Avatar.cpp
    ${MAINFOLDER}/source/Player.hpp
    ${MAINFOLDER}/source/Player.cpp
    ${MAINFOLDER}/source/World.hpp
    ${MAINFOLDER}/source/World.cpp
)

set(test_LIBS ${Boost_LIBRARIES} ${SFML_LIBRARIES} lua5.2 ${PHYSFS_LIBRARY})
set(test_BIN ${PROJECT_NAME}-unittests)

add_executable(${test_BIN} ${test_SRCS})
target_link_libraries(${test_BIN} ${test_LIBS})

add_custom_target(
    check ALL "${MAINFOLDER}/bin/${test_BIN}" 
    DEPENDS ${test_BIN} COMMENT "Executing unit tests..." 
    VERBATIM SOURCES ${test_SRCS}
)

add_custom_target(
    test "${MAINFOLDER}/bin/${test_BIN}" 
    DEPENDS ${test_BIN} COMMENT "Executing unit tests..." 
    VERBATIM SOURCES ${test_SRCS}
)
