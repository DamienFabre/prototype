-- Fichier de configuration de l'application

-- Window
width          = 800
height         = 600
title          = "Prototype"
vsync          = true
framerateLimit = 200
fullscreen     = false

-- autre
screenshotFormat = ".jpg"

-- Utilisateur
-- playerName = os.getenv("LOGNAME") or "player"
-- language   = os.getenv("LANG") or "fr" -- TODO modifier cette ligne